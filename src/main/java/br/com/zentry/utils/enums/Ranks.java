package br.com.zentry.utils.enums;

import net.md_5.bungee.api.ChatColor;

public enum Ranks {
	
	UNRANKED("-", ChatColor.WHITE, 0),
	SWIG("☰", ChatColor.GREEN, 1000),
	ZENTRY("✫", ChatColor.DARK_RED, 10000);

	private String prefix;
	private ChatColor color;
	private int xp;
	
	Ranks(String prefix, ChatColor color, int xp) {
		this.prefix = prefix;
		this.color = color;
		this.xp = xp;
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	public ChatColor getColor() {
		return color;
	}
	
	public int getXp() {
		return xp;
	}
}

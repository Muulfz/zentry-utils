package br.com.zentry.utils.enums;

import net.md_5.bungee.api.ChatColor;

public enum Tags {
	
	MEMBRO(ChatColor.GRAY, false),
	LIGHT(ChatColor.GREEN, true),
	MVP(ChatColor.BLUE, true),
	PRO(ChatColor.GOLD, true),
	YOUTUBER(ChatColor.AQUA, true),
	BUILDER(ChatColor.DARK_GREEN, true),
	BUILDERPLUS(ChatColor.DARK_GREEN, true),
	STAFF(ChatColor.YELLOW, true),
	YOUTUBERPLUS(ChatColor.DARK_AQUA, true),
	MOD(ChatColor.DARK_PURPLE, true),
	MODPLUS(ChatColor.DARK_PURPLE, true),
	ADMIN(ChatColor.RED, true),
	DEVELOPER(ChatColor.DARK_AQUA, true),
	DONO(ChatColor.DARK_RED, true);
	
	private ChatColor color;
	private boolean prefix;
	
	Tags(ChatColor color, boolean prefix) {
		this.color = color;
		this.prefix = prefix;
	}
	
	public ChatColor getColor() {
		return color;
	}
	
	public boolean isPrefix() {
		return prefix;
	}
	
	public String getPermission() {
		return "zentry.tag." + this.name().toLowerCase();
	}
}

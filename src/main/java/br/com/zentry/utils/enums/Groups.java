package br.com.zentry.utils.enums;

public enum Groups {
	
	MEMBRO(new String[]{
			//SPIGOT
			"zentry.tag.membro",
			//BUNGEECORD
	}),
	LIGHT(new String[]{
			//SPIGOT
			"zentry.tag.light",
			//BUNGEECORD
	}),
	MVP(new String[]{
			//SPIGOT
			"zentry.tag.mvp",
			//BUNGEECORD
	}),
	PRO(new String[]{
			//SPIGOT
			"zentry.tag.pro",
			//BUNGEECORD
	}),
	YOUTUBER(new String[]{
			//SPIGOT
			"zentry.tag.youtuber",
			//BUNGEECORD
	}),
	BUILDER(new String[]{
			//SPIGOT
			"zentry.tag.builder",
			//BUNGEECORD
	}),
	BUILDERPLUS(new String[]{
			//SPIGOT
			"zentry.tag.builderplus",
			//BUNGEECORD
	}),
	YOUTUBERPLUS(new String[]{
			//SPIGOT
			"zentry.tag.youtuberplus",
			"zentry.command.admin",
			"zentry.command.reports",
			"minecraft.command.clear",
			"minecraft.command.effect",
			"minecraft.command.enchant",
			"minecraft.command.gamemode",
			"minecraft.command.give",
			"minecraft.command.summon",
			"minecraft.command.time",
			"minecraft.command.toggledownfall",
			"minecraft.command.tp",
			//BUNGEECORD
			"zentry.bungee.command.punish",
			"zentry.bungee.command.server",
			"zentry.bungee.command.staffchat",
	}),
	MOD(new String[]{
			//SPIGOT
			"zentry.tag.mod",
			"zentry.command.admin",
			"zentry.command.reports",
			"minecraft.command.clear",
			"minecraft.command.effect",
			"minecraft.command.enchant",
			"minecraft.command.gamemode",
			"minecraft.command.give",
			"minecraft.command.summon",
			"minecraft.command.time",
			"minecraft.command.toggledownfall",
			"minecraft.command.tp",
			//BUNGEECORD
			"zentry.bungee.command.punish",
			"zentry.bungee.command.server",
			"zentry.bungee.command.staffchat",
	}),
	MODPLUS(new String[]{
			//SPIGOT
			"zentry.tag.modplus",
			"zentry.command.admin",
			"zentry.command.reports",
			"minecraft.command.clear",
			"minecraft.command.effect",
			"minecraft.command.enchant",
			"minecraft.command.gamemode",
			"minecraft.command.give",
			"minecraft.command.summon",
			"minecraft.command.time",
			"minecraft.command.toggledownfall",
			"minecraft.command.tp",
			//BUNGEECORD
			"zentry.bungee.command.punish",
			"zentry.bungee.command.server",
			"zentry.bungee.command.staffchat",
	}),
	ADMIN(new String[]{
			//SPIGOT
			"zentry.tag.admin",
			"zentry.command.admin",
			"zentry.command.reports",
			"zentry.command.youtubers",
			"minecraft.command.clear",
			"minecraft.command.difficulty",
			"minecraft.command.effect",
			"minecraft.command.enchant",
			"minecraft.command.gamemode",
			"minecraft.command.give",
			"minecraft.command.summon",
			"minecraft.command.tellraw",
			"minecraft.command.time",
			"minecraft.command.toggledownfall",
			"minecraft.command.tp",
			"minecraft.command.weather",
			"minecraft.command.whitelist",
			//BUNGEECORD
			"zentry.bungee.command.account",
			"zentry.bungee.command.account.permissions",
			"zentry.bungee.punishment",
			"zentry.bungee.command.punish",
			"zentry.bungee.command.unpunish",
			"zentry.bungee.command.server",
			"zentry.bungee.command.staffchat",
	}),
	DEVELOPER(new String[]{
			//SPIGOT
			"zentry.tag.developer",
			"zentry.command.admin",
			"zentry.command.reports",
			"zentry.command.youtubers",
			"minecraft.command.clear",
			"minecraft.command.difficulty",
			"minecraft.command.effect",
			"minecraft.command.enchant",
			"minecraft.command.gamemode",
			"minecraft.command.give",
			"minecraft.command.summon",
			"minecraft.command.tellraw",
			"minecraft.command.time",
			"minecraft.command.toggledownfall",
			"minecraft.command.tp",
			"minecraft.command.weather",
			"minecraft.command.whitelist",
			//BUNGEECORD
			"zentry.bungee.command.account",
			"zentry.bungee.command.account.permissions",
			"zentry.bungee.punishment",
			"zentry.bungee.command.punish",
			"zentry.bungee.command.unpunish",
			"zentry.bungee.command.server",
			"zentry.bungee.command.staffchat",
	}),
	DONO(new String[]{
			//SPIGOT
			"*",
			//BUNGEECORD
			"zentry.bungee.command.account",
			"zentry.bungee.command.account.permissions",
			"zentry.bungee.punishment",
			"zentry.bungee.command.punish",
			"zentry.bungee.command.unpunish",
			"zentry.bungee.command.server",
			"zentry.bungee.command.staffchat",
	});
	
	private String[] permissions;
	
	Groups(String[] permissions) {
		this.permissions = permissions;
	}
	
	public String[] getPermissions() {
		return permissions;
	}
}

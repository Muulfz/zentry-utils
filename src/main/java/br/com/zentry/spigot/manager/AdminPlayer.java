package br.com.zentry.spigot.manager;

import org.bukkit.GameMode;
import org.bukkit.inventory.ItemStack;

public class AdminPlayer {
	
	private ItemStack[] content;
	private ItemStack[] armor;
	private int slot;
	private GameMode mode;
	private double health;
	
	public AdminPlayer(ItemStack[] content, ItemStack[] armor, int slot, GameMode mode, double health) {
		this.content = content; this.armor = armor;
		this.slot = slot; this.mode = mode;
		this.health = health;
	}
	
	public ItemStack[] getContent() {
		return content;
	}
	
	public ItemStack[] getArmor() {
		return armor;
	}
	
	public int getSlot() {
		return slot;
	}
	
	public GameMode getMode() {
		return mode;
	}
	
	public double getHealth() {
		return health;
	}
}

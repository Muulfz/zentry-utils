package br.com.zentry.spigot.manager.runnable;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import br.com.zentry.manager.builder.ItemBuilder;
import br.com.zentry.manager.youtuber.Youtuber;
import br.com.zentry.manager.youtuber.YoutuberManager;
import br.com.zentry.spigot.ZentryUtils;
import net.md_5.bungee.api.ChatColor;

public class YoutuberInventory implements Listener {
	
	public static HashMap<String, BukkitTask> tasks = new HashMap<>();
	
	public static void openInventory(Player player) {
		Inventory inventory = Bukkit.createInventory(null, 54, "Youtubers:");
		List<Youtuber> list = YoutuberManager.getYoutubers();
		if(list.size() <= 0) {
			inventory.setItem(22, new ItemBuilder(Material.PAPER, 1, 0).setDisplayName(ChatColor.RED + "Nenhuma aplica��o ativa!").build());
			player.openInventory(inventory);
			return;
		}
		boolean error = false;
		for(Youtuber youtuber : list) {
			String nickname = ZentryUtils.getInstance().getManager().getOnlineNickname(youtuber.getUuid());
			error = nickname == null;
			if(!error) inventory.addItem(ItemBuilder.head(1, nickname, ChatColor.YELLOW + nickname, "", ChatColor.WHITE + "ID do canal:", ChatColor.GREEN + youtuber.getChannel(), "", ChatColor.WHITE + "Aplicado em:", ChatColor.GREEN + new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(youtuber.getTime()), "", ChatColor.YELLOW + "Clique para mais informa��es!"));
		}
		if(error) inventory.addItem(new ItemBuilder(Material.PAPER, 1, 0).setDisplayName(ChatColor.RED + "Erro!").setLore(ChatColor.RED + "Um ou mais youtubers n�o puderam ser carregados!").build());
		player.openInventory(inventory);
		BukkitTask task = new BukkitRunnable() {
			@Override
			public void run() {
				for(ItemStack stack : inventory.getContents()) {
					ItemMeta meta = stack.getItemMeta();
					for(Youtuber youtuber : YoutuberManager.getYoutubersByUniqueId(ZentryUtils.getInstance().getManager().getCachedUuid(meta.getDisplayName().replace("�e", "")))) {
						meta.setLore(Arrays.asList("", ChatColor.WHITE + "Aplicado em:", ChatColor.GREEN + new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(youtuber.getTime()), "", ChatColor.YELLOW + "Clique para mais informa��es!"));
						stack.setItemMeta(meta);
						player.updateInventory();
					}
				}
			}
		}.runTaskTimer(ZentryUtils.getPlugin(), 0, 20);
		tasks.put(player.getName(), task);
		return;
	}
	
	@EventHandler
	public void inventoryCloseEvent(InventoryCloseEvent event) {
		if(!(event.getPlayer() instanceof Player)) return;
		if(event.getInventory().getTitle().equals("Reports:")) {
			Player player = (Player) event.getPlayer();
			if(tasks.containsKey(player.getName())) {
				tasks.get(player.getName()).cancel();
				tasks.remove(player.getName());
			}
		}
	}
	
	@EventHandler
	public void playerQuitEvent(PlayerQuitEvent event) {
		Player player = (Player) event.getPlayer();
		if(tasks.containsKey(player.getName())) {
			tasks.get(player.getName()).cancel();
			tasks.remove(player.getName());
		}
	}
	
	@EventHandler
	public void playerKickEvent(PlayerKickEvent event) {
		Player player = (Player) event.getPlayer();
		if(tasks.containsKey(player.getName())) {
			tasks.get(player.getName()).cancel();
			tasks.remove(player.getName());
		}
	}
}

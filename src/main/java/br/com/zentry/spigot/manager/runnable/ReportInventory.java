package br.com.zentry.spigot.manager.runnable;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.builder.ItemBuilder;
import br.com.zentry.manager.report.Report;
import br.com.zentry.manager.report.ReportManager;
import br.com.zentry.spigot.ZentryUtils;
import net.md_5.bungee.api.ChatColor;

public class ReportInventory implements Listener {
	
	public static HashMap<String, BukkitTask> tasks = new HashMap<>();
	
	public static void openInventory(Player player) {
		Inventory inventory = Bukkit.createInventory(null, 54, "Reports:");
		List<Report> list = ReportManager.getReports();
		if(list.size() <= 0) {
			inventory.setItem(22, new ItemBuilder(Material.PAPER, 1, 0).setDisplayName(ChatColor.RED + "Nenhum report ativo!").build());
			player.openInventory(inventory);
			return;
		} 
		for(Report report : list) inventory.addItem(ItemBuilder.head(1, report.getReported(), ChatColor.YELLOW + report.getReported(), "", ChatColor.WHITE + "Motivo:", ChatColor.GREEN + report.getReason(), "", ChatColor.WHITE + "Reportado em:", ChatColor.GREEN + new SimpleDateFormat("HH:mm:ss").format(report.getTime()), "", ChatColor.WHITE + "Expira em:", ChatColor.GREEN + UtilsManager.formatLong(report.getExpire()), "", ChatColor.YELLOW + "Clique para se teleportar at� o acusado!"));
		player.openInventory(inventory);
		BukkitTask task = new BukkitRunnable() {
			@Override
			public void run() {
				for(ItemStack stack : inventory.getContents()) {
					if(stack == null || !stack.hasItemMeta()) return;
					ItemMeta meta = stack.getItemMeta();
					for(Report report : ReportManager.getReportsByName(meta.getDisplayName().replace("�e", ""))) {
						meta.setLore(Arrays.asList("", ChatColor.WHITE + "Motivo:", ChatColor.GREEN + report.getReason(), "", ChatColor.WHITE + "Reportado em:", ChatColor.GREEN + new SimpleDateFormat("HH:mm:ss").format(report.getTime()), "", ChatColor.WHITE + "Expira em:", ChatColor.GREEN + UtilsManager.formatLong(report.getExpire()), "", ChatColor.YELLOW + "Clique para se teleportar at� o acusado!"));
						stack.setItemMeta(meta);
						player.updateInventory();
					}
				}
			}
		}.runTaskTimer(ZentryUtils.getPlugin(), 0, 20);
		tasks.put(player.getName(), task);
		return;
	}
	
	@EventHandler
	public void inventoryCloseEvent(InventoryCloseEvent event) {
		if(!(event.getPlayer() instanceof Player)) return;
		if(event.getInventory().getTitle().equals("Reports:")) {
			Player player = (Player) event.getPlayer();
			if(tasks.containsKey(player.getName())) {
				tasks.get(player.getName()).cancel();
				tasks.remove(player.getName());
			}
		}
	}
	
	@EventHandler
	public void playerQuitEvent(PlayerQuitEvent event) {
		Player player = (Player) event.getPlayer();
		if(tasks.containsKey(player.getName())) {
			tasks.get(player.getName()).cancel();
			tasks.remove(player.getName());
		}
	}
	
	@EventHandler
	public void playerKickEvent(PlayerKickEvent event) {
		Player player = (Player) event.getPlayer();
		if(tasks.containsKey(player.getName())) {
			tasks.get(player.getName()).cancel();
			tasks.remove(player.getName());
		}
	}
}

package br.com.zentry.spigot;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.report.ReportManager;
import br.com.zentry.spigot.commands.CommandBase;
import br.com.zentry.spigot.commands.TabCommandBase;
import br.com.zentry.utils.ClassGetter;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_12_R1.ChatComponentText;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle.EnumTitleAction;

public class ZentryUtils extends JavaPlugin implements PluginMessageListener, TabCompleter {
	
	private static ZentryUtils instance;
	public static ZentryUtils getInstance() {
		return instance;
	}
	
	private static Plugin plugin;
	public static Plugin getPlugin() {
		return plugin;
	}
	
	private UtilsManager manager;
	public UtilsManager getManager() {
		return manager;
	}
	
	@Override
	public void onLoad() {
		return;
	}
	
	@Override
	public void onEnable() {
		for(Player player : Bukkit.getServer().getOnlinePlayers()) {
			player.kickPlayer(ChatColor.RED + "Este servidor est� sendo reiniciado.");
		}
		instance = this; plugin = this;
		manager = new UtilsManager(false);
		loadClasses();
		if(pirate()) {
			manager.log(Level.SEVERE, "Pirataria detectada! Retorne o arquivo \"plugin.yml\" ao seu estado original e tente novamente.");
			Bukkit.getServer().getPluginManager().disablePlugin(this);
			return;
		}
		if(UtilsManager.getMysql().hasProblem()) {
			manager.log(Level.SEVERE, "Inicializacao interrompida pois um problema foi detectado ao tentar estabelecer conexao com o banco de dados.");
			Bukkit.getServer().getPluginManager().disablePlugin(this);
			return;
		}
		ReportManager.checkReports();
		return;
	}
	
	@Override
	public void onDisable() {
		HandlerList.unregisterAll();
		manager = null;
		plugin = null; instance = null;
		return;
	}
	
	public void loadClasses() {
		Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		Bukkit.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
		for(Class<?> classes : ClassGetter.getClassesForPackage(this, "br.com.zentry.manager.permissions")) {
			try {
				if(Listener.class.isAssignableFrom(classes)) {
					Listener listener = (Listener)classes.newInstance();
					Bukkit.getServer().getPluginManager().registerEvents(listener, this);
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		for(Class<?> classes : ClassGetter.getClassesForPackage(this, "br.com.zentry.spigot")) {
			try {
				if(Listener.class.isAssignableFrom(classes)) {
					Listener listener = (Listener)classes.newInstance();
					Bukkit.getServer().getPluginManager().registerEvents(listener, this);
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
			try {
				if(CommandBase.class.isAssignableFrom(classes) && !classes.equals(CommandBase.class)) {
					CommandBase command = (CommandBase)classes.newInstance();
					((CraftServer)Bukkit.getServer()).getCommandMap().register(command.getName(), command);
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
			try {
				if(TabCommandBase.class.isAssignableFrom(classes) && !classes.equals(TabCommandBase.class)) {
					TabCommandBase command = (TabCommandBase)classes.newInstance();
					getCommand(command.getName()).setExecutor(command);
					getCommand(command.getName()).setTabCompleter(command);
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return;
	}
	
	private boolean pirate() {
		return !Base64.getEncoder().encodeToString(plugin.getDescription().getName().getBytes()).equals("WmVudHJ5VXRpbHM=") ||
				!Base64.getEncoder().encodeToString(plugin.getDescription().getVersion().getBytes()).equals("MS4wLjA=");
	}
	
	private void tab(Player player) {
		PacketPlayOutPlayerListHeaderFooter tab = new PacketPlayOutPlayerListHeaderFooter();
		Object header = new ChatComponentText("\n   " + ChatColor.DARK_AQUA + ChatColor.BOLD + "ZENTRY" + ChatColor.WHITE + ChatColor.BOLD + "MC   \n");
		Object alternative = new ChatComponentText("\n   " + ChatColor.WHITE + ChatColor.BOLD + "ZENTRY" + ChatColor.DARK_AQUA + ChatColor.BOLD + "MC   \n");
		Object footer = new ChatComponentText("\n" + ChatColor.DARK_AQUA + "Website: " + ChatColor.WHITE + "zentry.com.br\n" + ChatColor.DARK_AQUA + "Twitter: " + ChatColor.WHITE + "@ZentryMC\n");
		new BukkitRunnable() {
			boolean tabChanged = false;
			public void run() {
				try {
					Field a = tab.getClass().getDeclaredField("a");
					a.setAccessible(true);
					Field b = tab.getClass().getDeclaredField("b");
					b.setAccessible(true);
					if(tabChanged) {
						a.set(tab, alternative);
						tabChanged = false;
					} else {
						a.set(tab, header);
						tabChanged = true;
					}
					b.set(tab, footer);
					((CraftPlayer)player).getHandle().playerConnection.sendPacket(tab);
				} catch(Exception exception) {
					exception.printStackTrace();
					player.kickPlayer(ChatColor.RED + "N�o foi poss�vel enviar um packet � sua conex�o.");
				}
			}
		}.runTaskTimer(this, 0, 20);
	}
	
	public void welcome(Player player) {
		new BukkitRunnable() {
			public void run() {
				for(int i = 0; i < 100; i++) {
					player.sendMessage("");
				}
				tab(player);
				int hour = Integer.valueOf(new SimpleDateFormat("HH").format(new Date(new Timestamp(System.currentTimeMillis()).getTime())));
				String message = "Bom jogo";
				if(hour >= 19 && hour < 24) {
					message = "Boa noite";
				} else if(hour >= 12 && hour < 19) {
					message = "Boa tarde";
				} else if(hour >= 24 && hour < 12) {
					message = "Bom dia";
				}
				PacketPlayOutTitle title = new PacketPlayOutTitle(EnumTitleAction.TITLE, new ChatComponentText("" + ChatColor.DARK_AQUA + ChatColor.BOLD + "ZENTRY" + ChatColor.WHITE + ChatColor.BOLD + "MC"));
				((CraftPlayer)player).getHandle().playerConnection.sendPacket(title);
				PacketPlayOutTitle subTitle = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, new ChatComponentText(ChatColor.WHITE + message + ", " + ChatColor.DARK_AQUA + player.getName() + ChatColor.WHITE));
				((CraftPlayer)player).getHandle().playerConnection.sendPacket(subTitle);
				return;
			}
		}.runTaskLater(this, 10);
		new BukkitRunnable() {
			char[] split = "Seja bem-vindo ao servidor!".toCharArray();
			int counter = 0;
			String finalTitle = "";
			public void run() {
				if(!player.isOnline() || player == null) cancel();
				if(counter < split.length) {
					String letter = String.valueOf(split[counter]);
					counter++;
					finalTitle += letter;
					PacketPlayOutTitle packet = new PacketPlayOutTitle(EnumTitleAction.ACTIONBAR, new ChatComponentText(ChatColor.YELLOW + finalTitle));
					((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
				} else {
					cancel();
				}
			}
		}.runTaskTimer(ZentryUtils.getPlugin(), 0, 2);
	}
	
	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if(!channel.equals("BungeeCord")) return;
		ByteArrayDataInput input = ByteStreams.newDataInput(message);
		String subChannel = input.readUTF();
		if(subChannel.equals("teleport")) {
			String name = input.readUTF();
			if(Bukkit.getPlayer(name) == null) {
				player.sendMessage(ChatColor.RED + "Falha ao se teleportar: jogador n�o encontrado!");
			}
			new BukkitRunnable() {
				public void run() {
					player.teleport(Bukkit.getPlayer(name));
				}
			}.runTaskLater(this, 20);
		}
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		return Collections.emptyList();
	}
}
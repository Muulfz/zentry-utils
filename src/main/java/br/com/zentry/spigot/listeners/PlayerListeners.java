package br.com.zentry.spigot.listeners;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.player.ZentryPlayer;
import br.com.zentry.manager.report.Report;
import br.com.zentry.manager.report.ReportManager;
import br.com.zentry.manager.youtuber.Channel;
import br.com.zentry.manager.youtuber.Youtube;
import br.com.zentry.manager.youtuber.Youtuber;
import br.com.zentry.manager.youtuber.YoutuberManager;
import br.com.zentry.spigot.ZentryUtils;
import br.com.zentry.spigot.commands.AdminCommand;
import br.com.zentry.spigot.commands.LogsCommand.Log;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class PlayerListeners implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.MONITOR)
	public void playerJoinEvent(PlayerJoinEvent event) {
		event.setJoinMessage(null);
		ZentryPlayer player = new ZentryPlayer(ZentryUtils.getInstance().getManager().getCachedUuid(event.getPlayer().getName()), ZentryUtils.getInstance().getManager());
		player.injectPermissions();
		ZentryUtils.getInstance().welcome(player.getPlayer());
		new BukkitRunnable() {
			public void run() {
				new ZentryPlayer(ZentryUtils.getInstance().getManager().getCachedUuid(event.getPlayer().getName()), ZentryUtils.getInstance().getManager()).getTagManager().findTag();
				for(Player players : Bukkit.getServer().getOnlinePlayers()) {
					ZentryPlayer online = new ZentryPlayer(ZentryUtils.getInstance().getManager().getCachedUuid(players.getName()), ZentryUtils.getInstance().getManager());
					online.getTagManager().setTag(online.getTagManager().getTag());
				}
			}
		}.runTaskLater(ZentryUtils.getPlugin(), 10);
		for(Player online : Bukkit.getOnlinePlayers()) {
			if(AdminCommand.admin.contains(online)) {
				if(player.getGroupManager().getGroup().ordinal() < new ZentryPlayer(ZentryUtils.getInstance().getManager().getCachedUuid(online.getName()), ZentryUtils.getInstance().getManager()).getGroupManager().getGroup().ordinal()) {
					player.getPlayer().hidePlayer(online);
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void playerQuitEvent(PlayerQuitEvent event) {
		event.setQuitMessage(null);
		ZentryPlayer player = new ZentryPlayer(ZentryUtils.getInstance().getManager().getCachedUuid(event.getPlayer().getName()), ZentryUtils.getInstance().getManager());
		player.unloadPermissions();
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void asyncPlayerChatEvent(AsyncPlayerChatEvent event) {
		event.setCancelled(true);
		ZentryPlayer player = new ZentryPlayer(ZentryUtils.getInstance().getManager().getCachedUuid(event.getPlayer().getName()), ZentryUtils.getInstance().getManager());
		TextComponent component = new TextComponent();
		component.addExtra(UtilsManager.buildComponent(ChatColor.GRAY + "[" + player.getStatsManager().getRank().getColor() + player.getStatsManager().getRank().getPrefix() + ChatColor.GRAY + "] ", new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.GREEN + "Este � o rank " + player.getStatsManager().getRank().getColor() + player.getStatsManager().getRank().name() + ChatColor.GREEN + "!\n" + ChatColor.GREEN + "Seu XP m�nimo � " + ChatColor.WHITE + NumberFormat.getInstance().format(player.getStatsManager().getRank().getXp()) + ChatColor.GREEN + ".")}), new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/rank " + player.getStatsManager().getRank().name().toLowerCase())));
		component.addExtra(UtilsManager.buildComponent((player.getTagManager().getTag().isPrefix() ? player.getTagManager().getTag().getColor() + "" + ChatColor.BOLD + player.getTagManager().getTag().name().replace("PLUS", "+") + " " + player.getTagManager().getTag().getColor() : player.getTagManager().getTag().getColor()) + player.getName(), new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.YELLOW + "Primeiro login: " + ChatColor.WHITE + player.getDataManager().formatFirstLogin() + "\n" + ChatColor.YELLOW + "Ultimo login: " + ChatColor.WHITE + player.getDataManager().formatLastLogin())}), new ClickEvent(ClickEvent.Action.RUN_COMMAND, "")));
		component.addExtra(UtilsManager.buildComponent(ChatColor.GRAY + ": " + ChatColor.WHITE + event.getMessage(), new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.GRAY + "Mensagem enviada �s " + ChatColor.WHITE + new SimpleDateFormat("HH:mm:ss").format(new Date(new Timestamp(System.currentTimeMillis()).getTime())))}), new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "")));
		for(Player online : Bukkit.getServer().getOnlinePlayers()) {
			online.spigot().sendMessage(component);
		}
	}
	
	@SuppressWarnings("resource")
	@EventHandler(priority=EventPriority.MONITOR)
	public void inventoryClickEvent(InventoryClickEvent event) {
		if(!(event.getWhoClicked() instanceof Player)) {
			return;
		}
		if(event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta()) {
			return;
		}
		Material type = event.getCurrentItem().getType();
		ItemMeta meta = event.getCurrentItem().getItemMeta();
		if(event.getInventory().getTitle().equals("Reports:")) {
			event.setCancelled(true);
			if(type.equals(Material.SKULL_ITEM) && (meta.hasDisplayName() && meta.hasLore())) {
				((Player)event.getWhoClicked()).closeInventory();
				String name = meta.getDisplayName().replace("�e", "");
				List<Report> list = ReportManager.getReportsByName(name);
				if(list.size() <= 0) {
					event.getWhoClicked().sendMessage(ChatColor.RED + "Este report j� foi removido da lista.");
					return;
				}
				for(Report report : list) {
					ReportManager.unregisterReport(report);
				}
				ByteArrayDataOutput output = ByteStreams.newDataOutput();
				output.writeUTF("teleport");
				output.writeUTF(name);
				((Player)event.getWhoClicked()).sendPluginMessage(ZentryUtils.getPlugin(), "BungeeCord", output.toByteArray());
				new BukkitRunnable() {
					public void run() {
						((Player)event.getWhoClicked()).chat("/tp " + name);
					}
				}.runTaskLater(ZentryUtils.getPlugin(), 20);
			}
		} else if(event.getInventory().getTitle().equals("Youtubers:")) {
			event.setCancelled(true);
			if(type.equals(Material.SKULL_ITEM) && (meta.hasDisplayName() && meta.hasLore())) {
				((Player)event.getWhoClicked()).closeInventory();
				String name = meta.getDisplayName().replace("�e", "");
				UUID uuid = ZentryUtils.getInstance().getManager().getCachedUuid(name);
				if(uuid == null) {
					event.getWhoClicked().sendMessage(ChatColor.RED + "N�o foi poss�vel retornar UUID do usu�rio solicitado.");
					return;
				}
				List<Youtuber> list = YoutuberManager.getYoutubersByUniqueId(uuid);
				if(list.size() <= 0) {
					event.getWhoClicked().sendMessage(ChatColor.RED + "Este aplica��o j� foi removida da lista.");
					return;
				}
				if(list.size() >= 2) {
					event.getWhoClicked().sendMessage(ChatColor.RED + "Mais de uma aplica��o foi criada com essa UUID.");
					return;
				}
				Youtuber youtuber = list.get(0);
				Channel channel = null;
				try {
					channel = Youtube.getChannel(youtuber.getChannel());
				} catch(NumberFormatException exception) {
					event.getWhoClicked().sendMessage(ChatColor.RED + "N�o foi poss�vel retornar alguma estat�stica deste canal.");
					YoutuberManager.unregisterApplication(youtuber);
					event.getWhoClicked().sendMessage(ChatColor.GREEN + "Exibindo informa��es sobre o canal de " + ChatColor.WHITE + name + ChatColor.GREEN + ":\n" + ChatColor.GREEN + "  Link: " + ChatColor.WHITE + "http://youtube.com/channel/" + youtuber.getChannel());
					return;
				} catch(Exception exception) {
					event.getWhoClicked().sendMessage(ChatColor.RED + "N�o foi poss�vel retornar informa��es do canal.");
					return;
				}
				YoutuberManager.unregisterApplication(youtuber);
				event.getWhoClicked().sendMessage(ChatColor.GREEN + "Exibindo informa��es sobre o canal " + ChatColor.WHITE + channel.getName() + ChatColor.GREEN + ":\n" + ChatColor.GREEN + "  Link: " + ChatColor.WHITE + "http://youtube.com/channel/" + youtuber.getChannel() + "\n" + ChatColor.GREEN + "  Inscritos: " + ChatColor.WHITE + NumberFormat.getInstance().format(channel.getSubscribers()) + "\n" + ChatColor.GREEN + "  Visualiza��es: " + ChatColor.WHITE + NumberFormat.getInstance().format(channel.getViews()) + "\n" + ChatColor.GREEN + "  V�deos: " + ChatColor.WHITE + NumberFormat.getInstance().format(channel.getVideos()));
				ByteArrayDataOutput output = ByteStreams.newDataOutput();
				output.writeUTF("youtube");
				output.writeUTF(name);
				((Player)event.getWhoClicked()).sendPluginMessage(ZentryUtils.getPlugin(), "BungeeCord", output.toByteArray());
			}
		} else if(event.getInventory().getTitle().contains("Logs de:")) {
			event.setCancelled(true);
			if(type.equals(Material.STAINED_GLASS_PANE) && (meta.hasDisplayName() && meta.hasLore())) {
				event.getWhoClicked().closeInventory();
				if(meta.getDisplayName().contains("text")) {
					File file = new File(UtilsManager.folder.getPath() + "/logs/" + event.getInventory().getTitle().replace("Logs de: ", "") + ".txt");
					List<Log> list = new ArrayList<>();
					try {
						BufferedReader reader = new BufferedReader(new FileReader(file));
						String line = reader.readLine();
						if(line == null) {
							event.getWhoClicked().sendMessage(ChatColor.RED + "O arquivo deste jogador n�o possui conte�do.");
							return;
						}
						String[] split = line.split(",");
						if(split.length <= 1) {
							event.getWhoClicked().sendMessage(ChatColor.RED + "O arquivo deste jogador n�o possui conte�do.");
							return;
						}
						for(String string : split) {
							String[] splitter = string.split(":");
							long time = Long.valueOf(splitter[0]); String name = splitter[1];
							Log log = new Log(time, name);
							list.add(log);
						}
					} catch(Exception exception) {
						event.getWhoClicked().sendMessage(ChatColor.RED + "N�o foi poss�vel retornar logs para o arquivo solicitado.");
					}
					for(Log log : list) {
						event.getWhoClicked().sendMessage(ChatColor.YELLOW + "Data: " + ChatColor.WHITE + new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(log.getTime()) + ChatColor.GRAY + " | " + ChatColor.YELLOW + "Comando: " + ChatColor.WHITE + log.getCommand());
					}
				} else if(meta.getDisplayName().contains("arquivo")) {
					File toRead = new File(UtilsManager.folder.getPath() + "/logs/" + event.getInventory().getTitle().replace("Logs de: ", "") + ".txt");
					File file = new File(UtilsManager.folder.getPath() + "/logs/formatted/" + event.getInventory().getTitle().replace("Logs de: ", "") + ".txt");
					List<Log> list = new ArrayList<>();
					try(BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
						BufferedReader reader = new BufferedReader(new FileReader(toRead));
						String line = reader.readLine();
						if(line == null) {
							event.getWhoClicked().sendMessage(ChatColor.RED + "O arquivo deste jogador n�o possui conte�do.");
							return;
						}
						String[] split = line.split(",");
						if(split.length <= 1) {
							event.getWhoClicked().sendMessage(ChatColor.RED + "O arquivo deste jogador n�o possui conte�do.");
							return;
						}
						for(String string : split) {
							String[] splitter = string.split(":");
							long time = Long.valueOf(splitter[0]); String name = splitter[1];
							Log log = new Log(time, name);
							list.add(log);
						}
						for(Log log : list) {
							writer.write("Data: " + new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(log.getTime()) + " | Comando: " + log.getCommand());
							writer.newLine();
						}
						event.getWhoClicked().sendMessage(ChatColor.GREEN + "Arquivo gerado com sucesso em \"" + ChatColor.WHITE + file.getPath() + ChatColor.GREEN + "\"!");
					} catch(Exception exception) {
						event.getWhoClicked().sendMessage(ChatColor.RED + "N�o foi poss�vel gerar logs no arquivo solicitado.");
					}
				}
			}
		}
	}
}

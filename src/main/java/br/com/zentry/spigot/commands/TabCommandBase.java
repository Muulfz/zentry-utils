package br.com.zentry.spigot.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.spigot.ZentryUtils;

public abstract class TabCommandBase implements CommandExecutor, TabCompleter {
	
	public UtilsManager manager = ZentryUtils.getInstance().getManager();
	
	private String usage;
	
	public TabCommandBase(String usage) {
		this.usage = usage;
	}
	
	public String getName() {
		return this.getClass().getSimpleName().replace("Command", "").toLowerCase();
	}
	
	public String getUsage() {
		return usage.replace("<command>", this.getClass().getSimpleName().replace("Command", "").toLowerCase());
	}
	
	public String getPermission() {
		return "zentry.command." + getName().toLowerCase();
	}
	
	public abstract boolean onCommand(CommandSender sender, Command command, String label, String[] args);
	public abstract List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args);
}

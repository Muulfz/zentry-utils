package br.com.zentry.spigot.commands;

import java.util.Arrays;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import br.com.zentry.manager.youtuber.Youtuber;
import br.com.zentry.manager.youtuber.YoutuberManager;
import br.com.zentry.spigot.ZentryUtils;
import br.com.zentry.spigot.manager.runnable.YoutuberInventory;
import net.md_5.bungee.api.ChatColor;

public class YoutubersCommand extends CommandBase {
	
	public YoutubersCommand() {
		super("youtubers", "Veja a lista de aplica��es para youtuber", "/<command>", Arrays.asList("youtuberapps"));
	}
	
	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		if(!(sender instanceof Player)) {
			List<Youtuber> list = YoutuberManager.getYoutubers();
			if(list.size() == 0) {
				sender.sendMessage(ChatColor.RED + "Nenhuma aplica��o ativa no momento.");
				return false;
			}
			sender.sendMessage(ChatColor.GRAY + "Aplica��es ativas: " + ChatColor.WHITE + list.size());
			StringBuilder builder = new StringBuilder();
			boolean error = false;
			for(Youtuber youtuber : list) {
				if(builder.length() > 0) builder.append(ChatColor.WHITE + ", ");
				String nickname = ZentryUtils.getInstance().getManager().getOnlineNickname(youtuber.getUuid());
				if(nickname == null) error = true;
				else builder.append(ChatColor.YELLOW + nickname);
			}
			sender.sendMessage(ChatColor.GREEN + "Jogadores aplicados: " + builder.toString().trim());
			if(error) sender.sendMessage(ChatColor.RED + "Uma ou mais aplica��es n�o puderam ser carregadas.");
			return true;
		}
		if(!sender.hasPermission(getPermission())) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para ver as aplica��es para YouTuber!");
			return false;
		}
		if(args.length == 0) {
			YoutuberInventory.openInventory((Player)sender);
			return true;
		} else {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return false;
		}
	}
}

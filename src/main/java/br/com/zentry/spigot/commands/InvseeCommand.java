package br.com.zentry.spigot.commands;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

public class InvseeCommand extends CommandBase {

	public InvseeCommand() {
		super("invsee", "Comando utilizado para vizualizar invent�rios.", "/<command>", Arrays.asList("inv"));
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Apenas jogadores podem abrir invent�rios.");
		}
		Player player = (Player) sender;
		if (!sender.hasPermission("zentry.command.invsee")) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para vizualizar invent�rios.");
			return true;
		}
		if (args.length == 0) {
			player.sendMessage(ChatColor.RED + "Utilize:  /invsee <player>");
		} 
		else 
		if (args.length == 1) {
		Player targetPlayer = player.getServer().getPlayer(args[0]);
		PlayerInventory targetPlayerInventory = targetPlayer.getInventory();
		player.openInventory(targetPlayerInventory);
		}
		return false;
	}
}

package br.com.zentry.spigot.commands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.builder.ItemBuilder;
import net.md_5.bungee.api.ChatColor;

public class LogsCommand extends TabCommandBase {
	
	public static class Log {
		
		private long time;
		private String command;
		
		public Log(long time, String command) {
			this.time = time;
			this.command = command;
		}
		
		public long getTime() {
			return time;
		}
		
		public String getCommand() {
			return command;
		}
	}
	
	public LogsCommand() {
		super("/<command> <player>");
	}
	
	@SuppressWarnings("resource")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!sender.hasPermission(getPermission())) {
			return false;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Utilize: " + getUsage());
			return false;
		}
		if(args.length == 1) {
			List<Log> list = new ArrayList<>();
			File file = new File(UtilsManager.folder.getPath() + "/logs/" + args[0] + ".txt");
			if(!file.exists()) {
				sender.sendMessage(ChatColor.RED + "Este jogador n�o possui logs registradas.");
				return false;
			}
			sender.sendMessage(ChatColor.GREEN + "Retornando logs para o arquivo \"" + ChatColor.WHITE + file.getName() + ChatColor.GREEN + "\".");
			try {
				BufferedReader reader = new BufferedReader(new FileReader(file));
				String line = reader.readLine();
				if(line == null) {
					sender.sendMessage(ChatColor.RED + "O arquivo deste jogador n�o possui conte�do.");
					return false;
				}
				String[] split = line.split(",");
				if(split.length <= 1) {
					sender.sendMessage(ChatColor.RED + "O arquivo deste jogador n�o possui conte�do.");
					return false;
				}
				for(String string : split) {
					String[] splitter = string.split(":");
					long time = Long.valueOf(splitter[0]); String name = splitter[1];
					Log log = new Log(time, name);
					list.add(log);
				}
			} catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar logs para o arquivo solicitado.");
				return false;
			}
			if(!(sender instanceof Player)) {
				for(Log log : list) {
					sender.sendMessage(ChatColor.YELLOW + "Data: " + ChatColor.WHITE + new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(log.getTime()) + ChatColor.GRAY + " | " + ChatColor.YELLOW + "Comando: " + ChatColor.WHITE + log.getCommand());
				}
				return true;
			}
			Inventory inventory = Bukkit.createInventory(null, 54, "Logs de: " + file.getName().replace(".txt", ""));
			inventory.setItem(52, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 14).setDisplayName(ChatColor.GREEN + "Converter para arquivo").setLore(ChatColor.YELLOW + "Clique para gerar um arquivo formatado.").build());
			inventory.setItem(53, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 0).setDisplayName(ChatColor.GREEN + "Converter para texto").setLore(ChatColor.YELLOW + "Clique para receber o log no chat.").build());
			int count = -1;
			for(Log log : list) {
				if(count >= 51) break;
				count++;
				inventory.setItem(count, new ItemBuilder(Material.PAPER, 1, 0).setDisplayName(ChatColor.GRAY + "Informa��es:").setLore(ChatColor.YELLOW + "Data: " + ChatColor.WHITE + new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(log.getTime()), ChatColor.YELLOW + "Comando: " + ChatColor.WHITE + log.getCommand()).build());
			}
			((Player)sender).openInventory(inventory);
			return true;
		}
		if(args.length >= 2) {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return false;
		}
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> list = new ArrayList<>();
		if(args.length == 0 || args.length >= 2) {
			return Collections.emptyList();
		}
		if(args.length == 1) {
			File directory = new File(UtilsManager.folder.getPath() + "/logs");
			File[] files = directory.listFiles();
			FileFilter filter = new FileFilter() {
				@Override
				public boolean accept(File file) {
					return !file.isDirectory();
				}
			};
			files = directory.listFiles(filter);
			for(File file : files) {
				if(file.getName().toLowerCase().replace(".txt", "").startsWith(args[0].toLowerCase())) list.add(file.getName().replace(".txt", ""));
			}
		}
		return list;
	}
}

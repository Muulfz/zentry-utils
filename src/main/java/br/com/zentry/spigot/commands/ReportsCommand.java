package br.com.zentry.spigot.commands;

import java.util.Arrays;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import br.com.zentry.manager.report.Report;
import br.com.zentry.manager.report.ReportManager;
import br.com.zentry.spigot.manager.runnable.ReportInventory;
import net.md_5.bungee.api.ChatColor;

public class ReportsCommand extends CommandBase {
	
	public ReportsCommand() {
		super("reports", "Veja a lista de reports ativos", "/<command> [mode]", Arrays.asList("reportes"));
	}
	
	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		if(!(sender instanceof Player)) {
			List<Report> list = ReportManager.getReports();
			if(list.size() == 0) {
				sender.sendMessage(ChatColor.RED + "Nenhum report ativo no momento!");
				return false;
			}
			sender.sendMessage(ChatColor.GRAY + "Reports ativos: " + ChatColor.WHITE + list.size());
			StringBuilder builder = new StringBuilder();
			for(Report report : list) {
				if(builder.length() > 0) builder.append(ChatColor.WHITE + ", ");
				builder.append(ChatColor.YELLOW + report.getReported());
			}
			sender.sendMessage(ChatColor.GREEN + "Jogadores reportados: " + builder.toString().trim());
			return true;
		}
		if(!sender.hasPermission(getPermission())) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para ver os reports!");
			return false;
		}
		if(args.length == 0) {
			ReportInventory.openInventory((Player)sender);
			return true;
		} else {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return false;
		}
	}
}

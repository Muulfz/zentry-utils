package br.com.zentry.spigot.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import br.com.zentry.manager.player.ZentryPlayer;
import br.com.zentry.spigot.ZentryUtils;
import br.com.zentry.spigot.manager.AdminPlayer;
import br.com.zentry.utils.enums.Groups;
import net.md_5.bungee.api.ChatColor;

public class AdminCommand extends CommandBase {
	
	public static ArrayList<Player> admin = new ArrayList<>();
	
	public static HashMap<Player, AdminPlayer> players = new HashMap<>();
	
	public AdminCommand() {
		super("admin", "Entrar no modo admin", "/<command>", Arrays.asList("adm"));
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Apenas jogadores podem entrar no modo admin.");
			return false;
		}
		Player player = (Player) sender;
		if(!sender.hasPermission("zentry.command.admin")) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para entrar no modo admin.");
			return false;
		}
		if(args.length == 0) {
			if(admin.contains(player)) {
				admin.remove(player);
				if(!players.containsKey(player)) {
					sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar seu usu�rio salvo antes de entrar no modo admin.");
				} else {
					player.getInventory().clear();
					player.getInventory().setContents(players.get(player).getContent());
					player.getInventory().setArmorContents(players.get(player).getArmor());
					player.getInventory().setHeldItemSlot(players.get(player).getSlot());
					player.updateInventory();
					player.setGameMode(players.get(player).getMode());
					player.setHealth(players.get(player).getHealth());
				}
				players.remove(player);
				for(Player online : Bukkit.getOnlinePlayers()) {
					online.showPlayer(player);
				}
				sender.sendMessage(ChatColor.RED + "Voc� saiu do modo admin.\n" + ChatColor.RED + "Voc� est� vis�vel para todos!");
				ByteArrayDataOutput output = ByteStreams.newDataOutput();
				output.writeUTF("admin(off)");
				player.sendPluginMessage(ZentryUtils.getPlugin(), "BungeeCord", output.toByteArray());
				return true;
			} else {
				admin.add(player);
				players.put(player, new AdminPlayer(player.getInventory().getContents(), player.getInventory().getArmorContents(), player.getInventory().getHeldItemSlot(), player.getGameMode(), player.getHealthScale()));
				Groups group = new ZentryPlayer(ZentryUtils.getInstance().getManager().getCachedUuid(sender.getName()), ZentryUtils.getInstance().getManager()).getGroupManager().getGroup();
				List<Groups> list = new ArrayList<>();
				for(Player online : Bukkit.getOnlinePlayers()) {
					if(new ZentryPlayer(ZentryUtils.getInstance().getManager().getCachedUuid(online.getName()), ZentryUtils.getInstance().getManager()).getGroupManager().getGroup().ordinal() < group.ordinal()) online.hidePlayer(player);
				}
				for(Groups groups : Groups.values()) {
					list.add(groups);
				}
				player.getInventory().clear();
				player.setGameMode(GameMode.CREATIVE);
				player.setHealth(20.0D);
				sender.sendMessage(ChatColor.GREEN + "Voc� entrou no modo admin!\n" + ChatColor.GREEN + "Voc� est� invis�vel para " + ChatColor.WHITE + list.get(group.ordinal() - 1) + ChatColor.GREEN + " e abaixo!");
				ByteArrayDataOutput output = ByteStreams.newDataOutput();
				output.writeUTF("admin(on)");
				player.sendPluginMessage(ZentryUtils.getPlugin(), "BungeeCord", output.toByteArray());
			}
			return true;
		} else {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return false;
		}
	}
}

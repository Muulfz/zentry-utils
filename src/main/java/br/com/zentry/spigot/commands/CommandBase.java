package br.com.zentry.spigot.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.spigot.ZentryUtils;

public abstract class CommandBase extends Command {
	
	public UtilsManager manager = ZentryUtils.getInstance().getManager();
	
	public CommandBase(String name, String description, String usage, List<String> aliases) {
		super(name, description, usage, aliases);
	}
	
	public abstract boolean execute(CommandSender sender, String label, String[] args);
	
	@Override
	public String getPermission() {
		return "zentry.command." + this.getClass().getSimpleName().replace("Command", "").toLowerCase();
	}
}

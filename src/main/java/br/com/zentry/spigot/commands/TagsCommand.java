package br.com.zentry.spigot.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import br.com.zentry.manager.player.ZentryPlayer;
import br.com.zentry.utils.enums.Tags;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class TagsCommand extends TabCommandBase {
	
	public TagsCommand() {
		super("/<command> <tag>");
	}
	
	@SuppressWarnings("deprecation")
	private BaseComponent buildComponent(ZentryPlayer player, Tags tag) {
		BaseComponent component = new TextComponent((tag.equals(Tags.MEMBRO) ? "" : ChatColor.WHITE + ", ") + tag.getColor() + StringUtils.capitalise(tag.name().replace("PLUS", "+").toLowerCase()));
		component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.YELLOW + "Exemplo: " + (tag.isPrefix() ? tag.getColor() + "" + ChatColor.BOLD + tag.name().replace("PLUS", "+") + " " + tag.getColor() + player.getName() : tag.getColor() + player.getName()) + "\n" + ChatColor.GREEN + "Clique para selecionar esta tag!")}));
		component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tag " + tag.name().toLowerCase()));
		return component;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Apenas jogadores podem utilizar este comando.");
			return false;
		}
		ZentryPlayer player = new ZentryPlayer(manager.getCachedUuid(sender.getName()), manager);
		if(args.length == 0) {
			if(player.getTagManager().getAvailableTags().size() <= 0) {
				sender.sendMessage(ChatColor.RED + "Voc� n�o possui nenhuma tag dispon�vel.");
				return false;
			}
			TextComponent component = new TextComponent(ChatColor.GREEN + "Suas tags: ");
			for(Tags tag : player.getTagManager().getAvailableTags()) {
				component.addExtra(buildComponent(player, tag));
			}
			player.getPlayer().spigot().sendMessage(component);
		}
		if(args.length == 1) {
			if(player.getTagManager().getAvailableTags().size() <= 0) {
				sender.sendMessage(ChatColor.RED + "Voc� n�o possui nenhuma tag dispon�vel.");
				return false;
			}
			List<Tags> matches = new ArrayList<>();
			Tags tag = null;
			for(Tags tags : player.getTagManager().getAvailableTags()) {
				if(tags.name().toLowerCase().equalsIgnoreCase(args[0].toLowerCase())) {
					tag = tags;
				} else if(tags.name().toLowerCase().startsWith(args[0].toLowerCase())) {
					matches.add(tags);
				}
			}
			if(matches.size() == 0 && tag == null) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel localizar a tag citada.");
				return false;
			} else if(tag != null || matches.size() == 1) {
				Tags result = tag != null ? tag : matches.get(0);
				if(result.equals(player.getTagManager().getTag())) {
					sender.sendMessage(ChatColor.RED + "Voc� j� est� utilizando esta tag.");
					return false;
				}
				player.getTagManager().setTag(result);
				sender.sendMessage(ChatColor.GREEN + "Tag " + ChatColor.WHITE + result.name() + ChatColor.GREEN + " selecionada com sucesso.");
				return false;
			} else {
				TextComponent component = new TextComponent(ChatColor.YELLOW + "Seja mais espec�fico! Segue uma lista com as possibilidades:\n");
				int count = 0;
				for(Tags tags : matches) {
					count++;
					BaseComponent extra = new TextComponent((count > 1 ? ChatColor.WHITE + ", " : "") + tags.getColor() + StringUtils.capitalise(tags.name().replace("PLUS", "+").toLowerCase()));
					extra.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.YELLOW + "Exemplo: " + (tags.isPrefix() ? tags.getColor() + "" + ChatColor.BOLD + tags.name().replace("PLUS", "+") + " " + tags.getColor() + player.getName() : tags.getColor() + player.getName()) + "\n" + ChatColor.GREEN + "Clique para selecionar esta tag!")}));
					extra.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tag " + tags.name().toLowerCase()));
					component.addExtra(extra);
				}
				player.getPlayer().spigot().sendMessage(component);
				return false;
			}
		}
		if(args.length >= 2) {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return false;
		}
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		List<String> list = new ArrayList<>();
		if(!(sender instanceof Player) || args.length == 0 || args.length >= 2) return null;
		ZentryPlayer player = new ZentryPlayer(manager.getCachedUuid(sender.getName()), manager);
		if(player.getTagManager().getAvailableTags().size() <= 0) return null;
		if(args.length == 1) {
			for(Tags tags : player.getTagManager().getAvailableTags()) {
				if(tags.name().toLowerCase().startsWith(args[0].toLowerCase())) list.add(tags.name().toLowerCase());
			}
		}
		return list;
	}
}

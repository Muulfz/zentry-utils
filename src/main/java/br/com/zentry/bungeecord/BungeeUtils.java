package br.com.zentry.bungeecord;

import java.util.Base64;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import br.com.zentry.bungeecord.commands.BungeeCommandBase;
import br.com.zentry.bungeecord.commands.ServerCommand;
import br.com.zentry.manager.UtilsManager;
import br.com.zentry.utils.ClassGetter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;

public class BungeeUtils extends Plugin {
	
	private static BungeeUtils instance;
	public static BungeeUtils getInstance() {
		return instance;
	}
	
	private static Plugin plugin;
	public static Plugin getPlugin() {
		return plugin;
	}
	
	private UtilsManager manager;
	public UtilsManager getManager() {
		return manager;
	}
	
	@Override
	public void onLoad() {
		return;
	}
	
	@Override
	public void onEnable() {
		for(ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
			player.disconnect(new BaseComponent[]{new TextComponent(ChatColor.RED + "Este servidor est� sendo reiniciado.")});
		}
		instance = this; plugin = this;
		UtilsManager.loadFiles();
		manager = new UtilsManager(true);
		loadClasses();
		if(pirate()) {
			manager.log(Level.SEVERE, "Pirataria detectada! Retorne o arquivo \"plugin.yml\" ao seu estado original e tente novamente.");
			ProxyServer.getInstance().stop();
			return;
		}
		if(UtilsManager.getMysql().hasProblem()) {
			manager.log(Level.SEVERE, "Inicializacao interrompida pois um problema foi detectado ao tentar estabelecer conexao com o banco de dados.");
			ProxyServer.getInstance().stop();
			return;
		}
		ProxyServer.getInstance().getScheduler().schedule(this, new Runnable() {
			@SuppressWarnings("deprecation")
			public void run() {
				ProxyServer.getInstance().broadcast(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "ZENTRY" + ChatColor.WHITE + ChatColor.BOLD + "MC" + ChatColor.GRAY + ": " + ChatColor.WHITE + UtilsManager.messages[new Random().nextInt(UtilsManager.messages.length)]);
			}
		}, 0, 5, TimeUnit.MINUTES);
		ProxyServer.getInstance().getScheduler().schedule(this, new Runnable() {
			@Override
			public void run() {
				for(Map.Entry<String, ServerInfo> entry : ProxyServer.getInstance().getServers().entrySet()) {
					ServerCommand.load(entry.getValue());
				}
			}
		}, 0, 1, TimeUnit.SECONDS);
		return;
	}
	
	@Override
	public void onDisable() {
		ProxyServer.getInstance().getPluginManager().unregisterListeners(this);
		ProxyServer.getInstance().getPluginManager().unregisterCommands(this);
		UtilsManager.getMysql().disconnect();
		manager = null;
		plugin = null; instance = null;
		return;
	}
	
	public void loadClasses() {
		for(Class<?> classes : ClassGetter.getClassesForPackage(this, "br.com.zentry.bungeecord")) {
			try {
				if(Listener.class.isAssignableFrom(classes)) {
					Listener listener = (Listener)classes.newInstance();
					ProxyServer.getInstance().getPluginManager().registerListener(this, listener);
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
			try {
				if(BungeeCommandBase.class.isAssignableFrom(classes) && !classes.equals(BungeeCommandBase.class)) {
					BungeeCommandBase command = (BungeeCommandBase)classes.newInstance();
					ProxyServer.getInstance().getPluginManager().registerCommand(this, command);
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return;
	}
	
	private boolean pirate() {
		return !Base64.getEncoder().encodeToString(plugin.getDescription().getName().getBytes()).equals("WmVudHJ5VXRpbHM=") ||
				!Base64.getEncoder().encodeToString(plugin.getDescription().getVersion().getBytes()).equals("MS4wLjA=");
	}
}

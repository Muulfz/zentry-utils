package br.com.zentry.bungeecord.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.zentry.bungeecord.BungeeUtils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ProxyListeners implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void proxyPingEvent(ProxyPingEvent event) {
		ServerPing ping = event.getResponse();
		ping.setDescription("Test!");
		List<String> lines = new ArrayList<>();
		lines.add("Test!");
		ServerPing.PlayerInfo[] sample = new ServerPing.PlayerInfo[lines.size()];
		for(int i = 0; i < lines.size(); i++) sample[i] = new ServerPing.PlayerInfo(lines.get(i), "");
		ping.getPlayers().setSample(sample);
		event.setResponse(ping);
		ProxyServer.getInstance().getScheduler().schedule(BungeeUtils.getPlugin(), new Runnable() {
			@Override
			public void run() {
				ping.setDescription("asd");
				event.setResponse(ping);
			}
		}, 5, TimeUnit.SECONDS);
	}
}

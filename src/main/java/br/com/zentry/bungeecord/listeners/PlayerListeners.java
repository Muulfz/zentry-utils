package br.com.zentry.bungeecord.listeners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.warrenstrange.googleauth.GoogleAuthenticator;

import br.com.zentry.bungeecord.BungeeUtils;
import br.com.zentry.bungeecord.commands.AuthenticationCommand;
import br.com.zentry.bungeecord.commands.StaffChatCommand;
import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.player.Authenticator;
import br.com.zentry.manager.player.ZentryPlayer;
import br.com.zentry.utils.enums.Groups;
import br.com.zentry.utils.enums.Tags;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerListeners implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void loginEvent(LoginEvent event) {
		ZentryPlayer player = new ZentryPlayer(event.getConnection().getUniqueId(), BungeeUtils.getInstance().getManager());
		if(player.getPunishmentManager().isPunished() && player.getPunishmentManager().isBan()) {
			if(player.getPunishmentManager().isTemporary()) {
				int seconds = (int)((player.getPunishmentManager().getExpire() - System.currentTimeMillis()) / 1000L);
				if(seconds > 0) {
					event.setCancelled(true);
					event.setCancelReason(ChatColor.RED + "Voc� est� banido temporariamente do servidor desde " + player.getPunishmentManager().formatTime() + "!\n\nMotivo: " + player.getPunishmentManager().getReason() + "\nIdentificador: " + player.getIdentifier() + "\nExpira em: " + UtilsManager.formatLong(player.getPunishmentManager().getExpire()) + "\n\nPe�a sua revis�o em nosso f�rum!\nCompre seu unban em nossa loja e tenha seu acesso liberado!");
					return;
				}
				event.setCancelled(false);
				player.getPunishmentManager().setBan(false); player.getPunishmentManager().setExpire(0);
				player.getPunishmentManager().setPunisher(null); player.getPunishmentManager().setReason(null);
				player.getPunishmentManager().setTemporary(false); player.getPunishmentManager().setTime(0);
				return;
			}
			event.setCancelled(true);
			event.setCancelReason(ChatColor.RED + "Voc� est� banido permanentemente do servidor desde " + player.getPunishmentManager().formatTime() + "!\n\nMotivo: " + player.getPunishmentManager().getReason() + "\nIdentificador: " + player.getIdentifier() + "\n\nPe�a sua revis�o em nosso f�rum!\nCompre seu unban em nossa loja e tenha seu acesso liberado!");
			return;
		}
	}
	
	@EventHandler
	public void postLoginEvent(PostLoginEvent event) {
		ZentryPlayer player = new ZentryPlayer(event.getPlayer().getUniqueId(), BungeeUtils.getInstance().getManager());
		player.createUser();
		player.injectPermissions();
		player.getDataManager().setLastLogin(new Timestamp(System.currentTimeMillis()).getTime());
		player.getDataManager().setLastSeen((long)0);
		Authenticator authenticator = new Authenticator(BungeeUtils.getInstance().getManager(), event.getPlayer().getUniqueId());
		if(player.getGroupManager().getGroup().ordinal() >= Groups.BUILDERPLUS.ordinal()) {
			authenticator.createData();
		} else {
			authenticator.deleteData();
		}
		if(authenticator.dataExists()) {
			ProxyServer.getInstance().getScheduler().schedule(BungeeUtils.getPlugin(), new Runnable() {
				@Override 
				public void run() {
					Authenticator.lock.add(event.getPlayer().getName());
					if(!authenticator.hasRegistered()) {
						player.sendMessage(ChatColor.YELLOW + "Seu c�digo do Google Authenticator �: " + ChatColor.WHITE + authenticator.getKey() + "\n" + ChatColor.GREEN + "Registre-o no aplicativo para obter seu c�digo de 6 d�gitos.\n" + ChatColor.GREEN + "Ap�s obter, forne�a-o no chat!");
					} else {
						player.sendMessage(ChatColor.RED + "Digite seu c�digo de 6 d�gitos para se autenticar.");
					}
				}
			}, 3, TimeUnit.SECONDS);
		}
		ProxyServer.getInstance().getScheduler().schedule(BungeeUtils.getPlugin(), new Runnable() {
			@Override
			public void run() {
			}
		}, 3, TimeUnit.SECONDS);
	}
	
	@EventHandler
	public void playerDisconnectEvent(PlayerDisconnectEvent event) {
		ZentryPlayer player = new ZentryPlayer(event.getPlayer().getUniqueId(), BungeeUtils.getInstance().getManager());
		player.unloadPermissions();
		player.getDataManager().setLastSeen(new Timestamp(System.currentTimeMillis()).getTime());
		Authenticator authenticator = new Authenticator(BungeeUtils.getInstance().getManager(), event.getPlayer().getUniqueId());
		authenticator.setLogged(false);
	}
	
	@SuppressWarnings("resource")
	@EventHandler
	public void chatEvent(ChatEvent event) {
		ZentryPlayer player = new ZentryPlayer(((ProxiedPlayer)event.getSender()).getUniqueId(), BungeeUtils.getInstance().getManager());
		if(event.isCommand()) {
			String log = new Timestamp(System.currentTimeMillis()).getTime() + ":" + event.getMessage();
			File file = new File(UtilsManager.folder.getPath() + "/logs/" + player.getName() + ".txt");
			try(FileWriter writer = new FileWriter(file, file.exists())) {
				BufferedReader reader = new BufferedReader(new FileReader(file));
				boolean first = true;
				if(reader.readLine() != null) first = false;
				writer.write((first ? "" : ",") + log);
			} catch(IOException exception) {
				exception.printStackTrace();
			}
		}
		if(Authenticator.password.contains(player.getName())) {
			event.setCancelled(true);
			if(event.getMessage().split(" ").length >= 2) {
				player.sendMessage(ChatColor.RED + "Sua mensagem cont�m mais espa�os que o necess�rio.");
				return;
			}
			if(event.getMessage().length() <= 3) {
				player.sendMessage(ChatColor.RED + "Sua senha � muito curta e pode ser considerada insegura.");
				return;
			} else if(event.getMessage().length() >= 16) {
				player.sendMessage(ChatColor.RED + "Sua senha � muito longa e pode causar danos ao banco de dados.");
				return;
			}
			Authenticator authenticator = new Authenticator(BungeeUtils.getInstance().getManager(), player.getUuid());
			Authenticator.password.remove(player.getName());
			authenticator.setRegistered(true);
			authenticator.setPassword(UtilsManager.crypt(event.getMessage()));
			for(int i = 0; i < 100; i++) {
				player.sendMessage("");
			}
			player.sendMessage(ChatColor.GREEN + "Registro completo! Tenha um bom jogo.");
			authenticator.setLogged(true);
			Authenticator.lock.remove(player.getName());
			Authenticator.password.remove(player.getName());
			return;
		}
		if(Authenticator.lock.contains(player.getName())) {
			for(String string : new AuthenticationCommand().getAliases()) {
				if(event.getMessage().toLowerCase().startsWith("/" + string)) {
					return;
				}
			}
			event.setCancelled(true);
			if(event.getMessage().split(" ").length >= 2) {
				player.sendMessage(ChatColor.RED + "Sua mensagem cont�m mais espa�os que o necess�rio.");
				return;
			}
			int code = 0;
			try {
				code = Integer.valueOf(event.getMessage());
			} catch(Exception exception) {
				player.sendMessage(ChatColor.RED + "Seu c�digo de 6 d�gitos deve conter apenas n�meros.");
				return;
			}
			if(String.valueOf(code).length() >= 7) {
				player.sendMessage(ChatColor.RED + "Seu c�digo de 6 d�gitos deve conter apenas 6 d�gitos.");
				return;
			} else if(String.valueOf(code).length() <= 5) {
				player.sendMessage(ChatColor.RED + "Seu c�digo de 6 d�gitos deve conter no m�nimo 6 d�gitos.");
				return;
			}
			Authenticator authenticator = new Authenticator(BungeeUtils.getInstance().getManager(), player.getUuid());
			boolean access = new GoogleAuthenticator().authorize(authenticator.getKey(), code);
			if(!access) {
				player.sendMessage(ChatColor.RED + "C�digo inv�lido ou expirado. Tente novamente!");
				return;
			}
			Authenticator.lock.remove(player.getName());
			if(authenticator.getPassword() == null || authenticator.getPassword().equals("null")) {
				Authenticator.password.add(player.getName());
				player.sendMessage(ChatColor.GREEN + "Digite uma senha v�lida de recupera��o.");
				return;
			}
			for(int i = 0; i < 100; i++) {
				player.sendMessage("");
			}
			player.sendMessage(ChatColor.GREEN + "Autentica��o completa! Tenha um bom jogo.");
			authenticator.setLogged(true);
			Authenticator.lock.remove(player.getName());
			Authenticator.password.remove(player.getName());
			return;
		}
		if(StaffChatCommand.staffChat.contains(player.getProxiedPlayer()) && !event.getMessage().startsWith("/")) {
			event.setCancelled(true);
			if(StaffChatCommand.ignore.contains(player.getProxiedPlayer())) {
				player.sendMessage(ChatColor.RED + "Deixe de ignorar o staffchat para mandar mensagens.");
				return;
			}
			Tags tag = Tags.valueOf(player.getGroupManager().getGroup().name());
			for(ProxiedPlayer online : ProxyServer.getInstance().getPlayers()) {
				if(!event.getMessage().isEmpty() && online.hasPermission("zentry.bungee.command.staffchat") && !StaffChatCommand.ignore.contains(online)) {
					TextComponent component = new TextComponent(StaffChatCommand.prefix);
					component.addExtra(UtilsManager.buildComponent(tag.getColor() + "" + ChatColor.BOLD + tag.name() + " " + tag.getColor() + player.getName(), new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.YELLOW + "Nada a ser informado.")}), new ClickEvent(ClickEvent.Action.RUN_COMMAND, "")));
					component.addExtra(UtilsManager.buildComponent(ChatColor.WHITE + ": " + ChatColor.GRAY + event.getMessage().trim(), new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.GRAY + "Mensagem enviada em: " + ChatColor.WHITE + new SimpleDateFormat("HH:mm:ss").format(new Date(new Timestamp(System.currentTimeMillis()).getTime())))}), new ClickEvent(ClickEvent.Action.RUN_COMMAND, "")));
					online.sendMessage(component);
				}
			}
			return;
		}
		if(player.getPunishmentManager().isPunished() && !player.getPunishmentManager().isBan() && !event.getMessage().startsWith("/")) {
			if(player.getPunishmentManager().isTemporary()) {
				int seconds = (int)((player.getPunishmentManager().getExpire() - System.currentTimeMillis()) / 1000L);
				if(seconds > 0) {
					event.setCancelled(true);
					player.sendMessage(ChatColor.RED + "Voc� est� mutado temporariamente, utilize /account para mais informa��es." + "\n" + ChatColor.RED + "Seu mute expira em: " + UtilsManager.formatLong(player.getPunishmentManager().getExpire()));
					return;
				}
				player.getPunishmentManager().setBan(false); player.getPunishmentManager().setExpire(0);
				player.getPunishmentManager().setPunisher(null); player.getPunishmentManager().setReason(null);
				player.getPunishmentManager().setTemporary(false); player.getPunishmentManager().setTime(0);
				return;
			}
			event.setCancelled(true);
			player.sendMessage(ChatColor.RED + "Voc� est� mutado permanentemente, utilize /account para mais informa��es.");
			return;
		}
	}
}

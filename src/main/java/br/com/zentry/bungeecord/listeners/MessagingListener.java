package br.com.zentry.bungeecord.listeners;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class MessagingListener implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void pluginMessageEvent(PluginMessageEvent event) {
		if(!event.getTag().equals("BungeeCord")) {
			return;
		}
		ByteArrayDataInput input = ByteStreams.newDataInput(event.getData());
		String subChannel = input.readUTF();
		ProxiedPlayer player = ProxyServer.getInstance().getPlayer(event.getReceiver().toString());
		if(subChannel.equals("teleport")) {
			ProxiedPlayer target = ProxyServer.getInstance().getPlayer(input.readUTF());
			if(target == null) {
				player.sendMessage(ChatColor.RED + "Este jogador est� offline!");
				return;
			}
			if(!player.getServer().getInfo().equals(target.getServer().getInfo())) player.connect(target.getServer().getInfo());
		} else if(subChannel.equals("youtube")) {
			ProxiedPlayer target = ProxyServer.getInstance().getPlayer(input.readUTF());
			if(target != null) target.sendMessage(ChatColor.GREEN + "Sua aplica��o para YouTuber est� sendo analisada por um administrador!");
		}
	}
}

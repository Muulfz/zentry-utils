package br.com.zentry.bungeecord.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ServerCommand extends BungeeCommandBase {
	
	public ServerCommand() {
		super("server", "", "connet");
	}
	
	public static Map<ServerInfo, HashMap<Integer, Boolean>> servers = new HashMap<>();
	
	public static void load(ServerInfo info) {
		info.ping(new Callback<ServerPing>() {
			@Override
			public void done(ServerPing ping, Throwable throwable) {
				boolean online = (throwable == null);
				HashMap<Integer, Boolean> map = new HashMap<>();
				if(online) map.put(ping.getPlayers().getOnline(), online);
				else map.put(0, online);
				servers.put(info, map);
				return;
			}
		});
		return;
	}
	
	private static List<ServerInfo> getOnlineServers() {
		List<ServerInfo> list = new ArrayList<>();
		for(ServerInfo info : servers.keySet()) {
			for(Map.Entry<Integer, Boolean> entry : servers.get(info).entrySet()) {
				if(entry.getValue()) list.add(info);
			}
		}
		return list;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Apenas jogadores podem trocar de servidor.");
			return;
		}
		if(!sender.hasPermission("zentry.bungee.command.server")) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para ver a lista de servidores.");
			return;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.GRAY + "Servidores online: " + ChatColor.WHITE + getOnlineServers().size() + ChatColor.GRAY + "/" + ChatColor.WHITE + servers.size());
			boolean first = true;
			TextComponent component = new TextComponent();
			for(Map.Entry<ServerInfo, HashMap<Integer, Boolean>> entry : servers.entrySet()) {
				ServerInfo info = entry.getKey();
				for(Map.Entry<Integer, Boolean> entrySet : entry.getValue().entrySet()) {
					int players = entrySet.getKey();
					boolean online = entrySet.getValue();
					BaseComponent extra = new TextComponent(first ? (online ? ChatColor.GREEN : ChatColor.RED) + info.getName() : ChatColor.WHITE + ", " + (online ? ChatColor.GREEN : ChatColor.RED) + info.getName());
					extra.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(online ? "Jogadores: " + ChatColor.YELLOW + players + "\n" + ChatColor.GREEN + "Clique para se conectar!" : ChatColor.RED + "Servidor offline!")}));
					extra.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, online ? "/server " + info.getName() : ""));
					component.addExtra(extra);
					first = false;
				}
			}
			((ProxiedPlayer)sender).sendMessage(component);
			return;
		}
		if(args.length == 1) {
			ServerInfo info = ProxyServer.getInstance().getServerInfo(args[0]);
			if(info == null) {
				sender.sendMessage(ChatColor.RED + "Servidor inexistente.");
				return;
			}
			info.ping(new Callback<ServerPing>() {
				@Override
				public void done(ServerPing ping, Throwable throwable) {
					if(((ProxiedPlayer)sender).getServer().getInfo().equals(info)) {
						sender.sendMessage(ChatColor.RED + "Voc� j� est� conectado a este servidor.");
						return;
					}
					if(throwable == null) {
						((ProxiedPlayer)sender).connect(info);
						return;
					}
					sender.sendMessage(ChatColor.RED + "Servidor offline ou indispon�vel.");
					return;
				}
			});
			return;
		}
		if(args.length >= 2) {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return;
		}
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		List<String> list = new ArrayList<>();
		if(args.length == 0 || args.length >= 2) {
			return Collections.emptyList();
		}
		if(args.length == 1) {
			for(Map.Entry<String, ServerInfo> entry : ProxyServer.getInstance().getServers().entrySet()) {
				if(entry.getKey().toLowerCase().startsWith(args[0].toLowerCase())) {
					list.add(entry.getKey());
				}
			}
		}
		return list;
	}
}

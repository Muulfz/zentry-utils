package br.com.zentry.bungeecord.commands;

import java.util.Collections;

import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class LobbyCommand extends BungeeCommandBase {
	
	public LobbyCommand() {
		super("lobby", "", "hub");
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Apenas jogadores podem acessar o lobby.");
			return;
		}
		if(args.length != 0) {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer) sender;
		ServerInfo info = ProxyServer.getInstance().getServerInfo("lobby");
		info.ping(new Callback<ServerPing>() {
			@Override
			public void done(ServerPing ping, Throwable throwable) {
				if(player.getServer().getInfo().equals(info)) {
					player.sendMessage(ChatColor.RED + "Voc� j� est� conectado ao lobby.");
					return;
				}
				if(throwable == null) {
					player.connect(info);
					return;
				}
				player.sendMessage(ChatColor.RED + "Servidor offline ou indispon�vel.");
			}
		});
		return;
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		return Collections.emptyList();
	}
}

package br.com.zentry.bungeecord.commands;

import java.util.Collections;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PingCommand extends BungeeCommandBase {
	
	public PingCommand() {
		super("ping", "", "latency", "connection");
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			return;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.GREEN + "Seu ping: " + ChatColor.WHITE + ((ProxiedPlayer)sender).getPing() + "ms" + ChatColor.GREEN + "!");
			return;
		} else {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return;
		}
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		return Collections.emptyList();
	}
}

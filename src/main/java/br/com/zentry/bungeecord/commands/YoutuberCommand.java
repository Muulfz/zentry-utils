package br.com.zentry.bungeecord.commands;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import br.com.zentry.manager.youtuber.Channel;
import br.com.zentry.manager.youtuber.Video;
import br.com.zentry.manager.youtuber.Youtube;
import br.com.zentry.manager.youtuber.Youtuber;
import br.com.zentry.manager.youtuber.YoutuberManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class YoutuberCommand extends BungeeCommandBase {
	
	public YoutuberCommand() {
		super("youtuber", "", "youtube", "yt");
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Apenas jogadores podem se aplicar para youtuber!");
			return;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Utilize: /" + getName() + " <delete/channel>");
			return;
		}
		if(args.length == 1) {
			if(args[0].toLowerCase().equals("delete")) {
				List<Youtuber> list = YoutuberManager.getYoutubersByUniqueId(((ProxiedPlayer)sender).getUniqueId());
				if(list.size() <= 0) {
					sender.sendMessage(ChatColor.RED + "Voc� n�o possui aplica��es ativas.");
					return;
				}
				for(Youtuber youtuber : list) YoutuberManager.unregisterApplication(youtuber);
				sender.sendMessage(ChatColor.RED + "Suas aplica��es ativas foram deletadas.");
				return;
			}
			if(YoutuberManager.getYoutubersByUniqueId(((ProxiedPlayer)sender).getUniqueId()).size() > 0) {
				sender.sendMessage(ChatColor.RED + "Voc� j� se aplicou para youtuber!");
				return;
			}
			UUID uuid = ((ProxiedPlayer)sender).getUniqueId(); String id = args[0]; long time = new Timestamp(System.currentTimeMillis()).getTime();
			try {
				if(!Youtube.channelExists(id)) {
					sender.sendMessage(ChatColor.RED + "N�o foi poss�vel localizar o canal citado.");
					return;
				}
				List<Video> list = new ArrayList<>();
				for(Video video : Youtube.getVideosByChannel(id)) {
					if(video.getTitle().toLowerCase().contains("#zentrymc")) list.add(video);
				}
				if(list.size() <= 0) {
					sender.sendMessage(ChatColor.RED + "Voc� precisa ter um ou mais v�deos gravados no servidor com a tag \"#ZENTRYMC\" no t�tulo.");
					return;
				}
				Channel channel = Youtube.getChannel(id);
				if(channel.getSubscribers() < 5000 || channel.getVideos() < 10 || channel.getViews() < 15000) {
					sender.sendMessage(ChatColor.RED + "Seu canal n�o cumpre com os requisitos m�nimos.");
					return;
				}
				if(!channel.getDescription().toLowerCase().contains("#" + sender.getName().toLowerCase())) {
					sender.sendMessage(ChatColor.RED + "A descri��o do seu canal deve conter a tag \"#" + sender.getName() + "\".");
					return;
				}
				if(channel.hiddenSubscribers()) {
					sender.sendMessage(ChatColor.RED + "Seu canal n�o pode ter o n�mero de inscritos escondido.");
					return;
				}
			} catch(NumberFormatException exception) {
				exception.printStackTrace();
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar alguma das estat�sticas de seu canal.\n" + ChatColor.YELLOW + "Entre em contato com nossa equipe pelo Twitter/F�rum.");
				return;
			} catch(IOException exception) {
				exception.printStackTrace();
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar objetos da conex�o com o YouTube.\n" + ChatColor.YELLOW + "Entre em contato com nossa equipe pelo Twitter/F�rum.");
				return;
			} catch(Exception exception) {
				exception.printStackTrace();
				sender.sendMessage(ChatColor.RED + "Um erro desconhecido impossibilitou o registro de sua aplica��o.\n" + ChatColor.YELLOW + "Entre em contato com nossa equipe pelo Twitter/F�rum.");
				return;
			}
			Youtuber youtuber = new Youtuber(uuid, id, time);
			YoutuberManager.registerApplication(youtuber);
			sender.sendMessage(ChatColor.GREEN + "Aplica��o registrada com sucesso.");
			return;
		}
		if(args.length >= 2) {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return;
		}
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		if(args.length == 1) {
			new LinkedList<String>(Arrays.asList("delete"));
		}
		return Collections.emptyList();
	}
}

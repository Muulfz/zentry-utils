package br.com.zentry.bungeecord.commands;

import br.com.zentry.bungeecord.BungeeUtils;
import br.com.zentry.manager.UtilsManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public abstract class BungeeCommandBase extends Command implements TabExecutor {
	
	public UtilsManager manager = BungeeUtils.getInstance().getManager();
	
	public BungeeCommandBase(String name, String permission, String... aliases) {
		super(name, permission, aliases);
	}
	
	public abstract void execute(CommandSender sender, String[] args);
	public abstract Iterable<String> onTabComplete(CommandSender sender, String[] args);
}

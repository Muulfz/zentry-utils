package br.com.zentry.bungeecord.commands;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.report.Report;
import br.com.zentry.manager.report.ReportManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ReportCommand extends BungeeCommandBase {
	
	public ReportCommand() {
		super("report", "", "reportar", "reporte");
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Apenas jogadores podem reportar outros jogadores!");
			return;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Utilize: /" + getName() + " <player> <reason>");
			return;
		}
		if(args.length == 1) {
			ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
			if(player == null) {
				sender.sendMessage(ChatColor.RED + "Jogador n�o encontrado!");
				return;
			}
			sender.sendMessage(ChatColor.RED + "Utilize: /" + getName() + " " + player.getName() + " <reason>");
			return;
		}
		if(args.length >= 2) {
			ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
			if(player == null) {
				sender.sendMessage(ChatColor.RED + "Jogador n�o encontrado!");
				return;
			}
			StringBuilder builder = new StringBuilder();
			for(int i = 1; i < args.length; i++) {
				builder.append(args[i]).append(" ");
			}
			String name = player.getName(); String reason = builder.toString().trim(); long time = new Timestamp(System.currentTimeMillis()).getTime(); long expire = UtilsManager.stringToLong("10m");
			Report report = new Report(name, reason, time, expire);
			ReportManager.registerReport(report);
			sender.sendMessage(ChatColor.GREEN + "Jogador reportado com sucesso!");
			return;
		}
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		List<String> list = new ArrayList<>();
		if(args.length == 0 || args.length >= 2) {
			return Collections.emptyList();
		}
		for(ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
			if(player.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
				list.add(player.getName());
			}
		}
		return list;
	}
}

package br.com.zentry.bungeecord.commands;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


import br.com.zentry.manager.UtilsManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class StaffChatCommand extends BungeeCommandBase {
	
	public static ArrayList<ProxiedPlayer> staffChat = new ArrayList<>();
	public static ArrayList<ProxiedPlayer> ignore = new ArrayList<>();
	
	public StaffChatCommand() {
		super("staffchat", "", "staff", "sc");
	}
	
	public enum Modes {
		TOGGLE;
	}
	
	public static String prefix = ChatColor.WHITE + "[" + ChatColor.GREEN + "STAFF" + ChatColor.WHITE + "] ";
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			if(args.length == 0) {
				
			}
			if(args.length >= 1) {
				StringBuilder builder = new StringBuilder();
				for(int i = 0; i < args.length; i++) {
					builder.append(args[i]).append(" ");
				}
				ProxyServer.getInstance().getConsole().sendMessage(prefix + ChatColor.DARK_RED + sender.getName() + ChatColor.WHITE + ": " + ChatColor.GRAY + builder.toString().trim());
				for(ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
					if(player.hasPermission("zentry.bungee.command.staffchat")) {
						TextComponent component = new TextComponent(prefix);
						component.addExtra(UtilsManager.buildComponent(ChatColor.DARK_RED + sender.getName(), new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.YELLOW + "Nada a ser informado.")}), new ClickEvent(ClickEvent.Action.RUN_COMMAND, "")));
						component.addExtra(UtilsManager.buildComponent(ChatColor.WHITE + ": " + ChatColor.GRAY + builder.toString().trim(), new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.GRAY + "Mensagem enviada em: " + ChatColor.WHITE + new SimpleDateFormat("HH:mm:ss").format(new Date(new Timestamp(System.currentTimeMillis()).getTime())))}), new ClickEvent(ClickEvent.Action.RUN_COMMAND, "")));
						player.sendMessage(component);
					}
				}
			}
			return;
		}
		if(!sender.hasPermission("zentry.bungee.command.staffchat")) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para enviar mensagens no canal privado.");
			return;
		}
		if(args.length == 0) {
			if(staffChat.contains((ProxiedPlayer)sender)) {
				staffChat.remove((ProxiedPlayer)sender);
			} else {
				staffChat.add((ProxiedPlayer)sender);
			}
			sender.sendMessage(staffChat.contains((ProxiedPlayer)sender) ? ChatColor.GREEN + "Voc� entrou no staffchat." : ChatColor.RED + "Voc� saiu do staffchat.");
		}
		if(args.length == 1) {
			Modes mode = null;
			try {
				mode = Modes.valueOf(args[0].toUpperCase());
			} catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "Opera��o inv�lida. Insira um modo existente.");
				return;
			}
			switch(mode) {
			case TOGGLE:
				if(ignore.contains((ProxiedPlayer)sender)) {
					ignore.remove((ProxiedPlayer)sender);
				} else {
					ignore.add((ProxiedPlayer)sender);
				}
				sender.sendMessage(ignore.contains((ProxiedPlayer)sender) ? ChatColor.RED + "Voc� n�o receber� mais mensagens do staffchat." : ChatColor.GREEN + "Agora voc� receber� mensagens do staffchat.");
				return;
			default:
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar uma a��o para a op��o escolhida. #" + args[0]);
				return;
			}
		}
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		List<String> list = new ArrayList<>();
		if(!(sender instanceof ProxiedPlayer) || args.length == 0 || args.length >= 2) {
			return Collections.emptyList();
		}
		if(args.length == 1) {
			for(Modes mode : Modes.values()) {
				if(mode.name().toLowerCase().startsWith(args[0].toLowerCase())) {
					list.add(mode.name().toLowerCase());
				}
			}
		}
		return list;
	}
}

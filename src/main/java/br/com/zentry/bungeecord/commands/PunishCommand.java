package br.com.zentry.bungeecord.commands;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.player.ZentryPlayer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

@SuppressWarnings("deprecation")
public class PunishCommand extends BungeeCommandBase {
	
	public PunishCommand() {
		super("punish", "", "p", "punir");
	}
	
	public enum Modes {
		BAN, MUTE;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!sender.hasPermission("zentry.bungee.command.punish")) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para punir jogadores.");
			return;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Utilize: /" + getName() + " <type> <player> <time> <reason>");
			return;
		}
		if(args.length == 1) {
			Modes mode = null;
			try { mode = Modes.valueOf(args[0].toUpperCase()); } catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o tipo citado.");
				return;
			}
			sender.sendMessage(ChatColor.RED + "Utilize: " + getName() + " " + mode.name().toLowerCase() + " <player> <time> <reason>");
			return;
		}
		if(args.length == 2) {
			Modes mode = null;
			try { mode = Modes.valueOf(args[0].toUpperCase()); } catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o tipo citado.");
				return;
			}
			sender.sendMessage(ChatColor.RED + "Utilize: " + getName() + " " + mode.name().toLowerCase() + " " + args[1] + " <time> <reason>");
			return;
		}
		if(args.length == 3) {
			Modes mode = null;
			try { mode = Modes.valueOf(args[0].toUpperCase()); } catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o tipo citado.");
				return;
			}
			sender.sendMessage(ChatColor.RED + "Utilize: " + getName() + " " + mode.name().toLowerCase() + " " + args[1] + " " + args[2] + " <reason>");
		}
		if(args.length >= 4) {
			Modes mode = null;
			try { mode = Modes.valueOf(args[0].toUpperCase()); } catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o tipo citado.");
				return;
			}
			ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);
			boolean ban = mode.equals(Modes.BAN);
			boolean permanent = args[2].equalsIgnoreCase("n");
			long time = permanent ? 0 : UtilsManager.stringToLong(args[2]);
			if(time == 0 && !permanent) {
				sender.sendMessage(ChatColor.RED + "Formato de tempo incorreto. Utilize 1+d,1+h,1+m,1+s.");
				return;
			}
			StringBuilder builder = new StringBuilder();
			for(int i = 3; i < args.length; i++) builder.append(args[i]).append(" ");
			if(target == null) {
				UUID uuid = null;
				if((uuid = manager.getCachedUuid(args[1])) != null) {
					ZentryPlayer player = new ZentryPlayer(uuid, manager);
					if(!player.userExists()) {
						sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
						return;
					}
					if(player.getPunishmentManager().isPunished()) {
						if(ban) {
							if(player.getPunishmentManager().isBan() && !player.getPunishmentManager().isTemporary()) {
								sender.sendMessage(ChatColor.RED + "Este jogador j� est� banido permanentemente.");
								return;
							}
						}
					}
					player.getPunishmentManager().setBan(ban); player.getPunishmentManager().setTemporary(!permanent);
					player.getPunishmentManager().setExpire(permanent ? 0 : time); player.getPunishmentManager().setReason(builder.toString().trim());
					player.getPunishmentManager().setPunisher(sender.getName()); player.getPunishmentManager().setTime(new Timestamp(System.currentTimeMillis()).getTime());
					sender.sendMessage(ChatColor.GREEN + "Voc� " + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
					for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
						if(players.hasPermission("zentry.bungee.punishment")) players.sendMessage(ChatColor.RED + sender.getName() + " " + (ban ? "baniu" : "mutou") + " " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.RED + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
					}
					return;
				} else if((uuid = manager.getOnlineUuid(args[1])) != null) {
					ZentryPlayer player = new ZentryPlayer(uuid, manager);
					if(!player.userExists()) {
						sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
						return;
					}
					if(player.getPunishmentManager().isPunished()) {
						if(ban) {
							if(player.getPunishmentManager().isBan() && !player.getPunishmentManager().isTemporary()) {
								sender.sendMessage(ChatColor.RED + "Este jogador j� est� banido permanentemente.");
								return;
							}
						}
					}
					player.getPunishmentManager().setBan(ban); player.getPunishmentManager().setTemporary(!permanent);
					player.getPunishmentManager().setExpire(permanent ? 0 : time); player.getPunishmentManager().setReason(builder.toString().trim());
					player.getPunishmentManager().setPunisher(sender.getName()); player.getPunishmentManager().setTime(new Timestamp(System.currentTimeMillis()).getTime());
					sender.sendMessage(ChatColor.GREEN + "Voc� " + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
					for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
						if(players.hasPermission("zentry.bungee.punishment")) players.sendMessage(ChatColor.RED + sender.getName() + " " + (ban ? "baniu" : "mutou") + " " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.RED + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
					}
					return;
				} else if((uuid = manager.formatUuid(args[1])) != null) {
					ZentryPlayer player = new ZentryPlayer(uuid, manager);
					if(!player.userExists()) {
						sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
						return;
					}
					if(player.getPunishmentManager().isPunished()) {
						if(ban) {
							if(player.getPunishmentManager().isBan() && !player.getPunishmentManager().isTemporary()) {
								sender.sendMessage(ChatColor.RED + "Este jogador j� est� banido permanentemente.");
								return;
							}
						}
					}
					player.getPunishmentManager().setBan(ban); player.getPunishmentManager().setTemporary(!permanent);
					player.getPunishmentManager().setExpire(permanent ? 0 : time); player.getPunishmentManager().setReason(builder.toString().trim());
					player.getPunishmentManager().setPunisher(sender.getName()); player.getPunishmentManager().setTime(new Timestamp(System.currentTimeMillis()).getTime());
					sender.sendMessage(ChatColor.GREEN + "Voc� " + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
					for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
						if(players.hasPermission("zentry.bungee.punishment")) players.sendMessage(ChatColor.RED + sender.getName() + " " + (ban ? "baniu" : "mutou") + " " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.RED + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
					}
					return;
				} else if((uuid = manager.getUuid(args[1])) != null) {
					ZentryPlayer player = new ZentryPlayer(uuid, manager);
					if(!player.userExists()) {
						sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
						return;
					}
					if(player.getPunishmentManager().isPunished()) {
						if(ban) {
							if(player.getPunishmentManager().isBan() && !player.getPunishmentManager().isTemporary()) {
								sender.sendMessage(ChatColor.RED + "Este jogador j� est� banido permanentemente.");
								return;
							}
						}
					}
					player.getPunishmentManager().setBan(ban); player.getPunishmentManager().setTemporary(!permanent);
					player.getPunishmentManager().setExpire(permanent ? 0 : time); player.getPunishmentManager().setReason(builder.toString().trim());
					player.getPunishmentManager().setPunisher(sender.getName()); player.getPunishmentManager().setTime(new Timestamp(System.currentTimeMillis()).getTime());
					sender.sendMessage(ChatColor.GREEN + "Voc� " + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
					for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
						if(players.hasPermission("zentry.bungee.punishment")) players.sendMessage(ChatColor.RED + sender.getName() + " " + (ban ? "baniu" : "mutou") + " " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.RED + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
					}
					return;
				} else {
					sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar UUID do jogador citado.");
				}
				return;
			}
			ZentryPlayer player = new ZentryPlayer(target.getUniqueId(), manager);
			if(!player.userExists()) {
				sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
				return;
			}
			if(player.getPunishmentManager().isPunished()) {
				if(ban) {
					if(player.getPunishmentManager().isBan() && !player.getPunishmentManager().isTemporary()) {
						sender.sendMessage(ChatColor.RED + "Este jogador j� est� banido permanentemente.");
						return;
					}
				}
			}
			player.getPunishmentManager().setBan(ban); player.getPunishmentManager().setTemporary(!permanent);
			player.getPunishmentManager().setExpire(permanent ? 0 : time); player.getPunishmentManager().setReason(builder.toString().trim());
			player.getPunishmentManager().setPunisher(sender.getName()); player.getPunishmentManager().setTime(new Timestamp(System.currentTimeMillis()).getTime());
			if(ban) player.kick(ChatColor.RED + "Voc� foi banido " + (permanent ? "permanentemente" : "temporariamente") + " do servidor!\n\nMotivo: " + builder.toString().trim() + "\nIdentificador: " + player.getIdentifier() + "\n\nPe�a sua revis�o em nosso f�rum!\nCompre seu unban em nossa loja e tenha seu acesso liberado!");
			else player.sendMessage(ChatColor.RED + "Voc� foi mutado " + (permanent ? "permanentemente" : "temporariamente") + " do servidor.\n\n" + ChatColor.RED + "Motivo: " + builder.toString().trim() + "\n" + ChatColor.RED + "Identificador: " + player.getIdentifier() + "\n\n" + ChatColor.RED + "Pe�a sua revis�o em nosso f�rum!\n\n" + ChatColor.RED + "Compre seu unban em nossa loja e tenha seu acesso liberado!");
			sender.sendMessage(ChatColor.GREEN + "Voc� " + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
			for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
				if(players.hasPermission("zentry.bungee.punishment")) players.sendMessage(ChatColor.RED + sender.getName() + " " + (ban ? "baniu" : "mutou") + " " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.RED + ") " + (permanent ? "permanentemente" : "temporariamente") + " por " + builder.toString().trim() + ".");
			}
			return;
		}
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		if(args.length == 0 || args.length >= 4) {
			return Collections.emptyList();
		}
		List<String> matches = new ArrayList<>();
		if(args.length == 1) {
			for(Modes mode : Modes.values()) {
				if(mode.name().toLowerCase().startsWith(args[0].toLowerCase())) matches.add(mode.name().toLowerCase());
			}
		}
		if(args.length == 2) {
			for(ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
				if(player.getName().toLowerCase().startsWith(args[1].toLowerCase())) matches.add(player.getName());
			}
		}
		if(args.length == 3) {
			String[] list = {"1d", "1h", "1m", "1s"};
			for(String string : list) matches.add(string);
		}
		return matches;
	}
}

package br.com.zentry.bungeecord.commands;

import java.util.Collections;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.player.Authenticator;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class AuthenticationCommand extends BungeeCommandBase {
	
	public AuthenticationCommand() {
		super("authentication", "", "auth", "authenticator", "autenticador");
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer) sender;
		Authenticator authenticator = new Authenticator(manager, player.getUniqueId());
		if(!authenticator.dataExists()) {
			sender.sendMessage(ChatColor.RED + "Seu autenticador n�o foi gerado.");
			return;
		}
		if(!authenticator.hasRegistered()) {
			sender.sendMessage(ChatColor.RED + "Seu autenticador n�o foi registrado.");
			return;
		}
		if(authenticator.getPassword() == null) {
			sender.sendMessage(ChatColor.RED + "Seu autenticador n�o possui uma senha de recupera��o.");
			return;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Utilize: /" + getName() + " <password>");
			return;
		}
		if(args.length == 1) {
			if(!UtilsManager.crypt(args[0]).equals(authenticator.getPassword())) {
				sender.sendMessage(ChatColor.RED + "Senha incorreta.");
				return;
			}
			sender.sendMessage(ChatColor.GREEN + "Seu c�digo secreto �: " + ChatColor.WHITE + authenticator.getKey());
			return;
		}
		if(args.length >= 2) {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return;
		}
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		return Collections.emptyList();
	}
}
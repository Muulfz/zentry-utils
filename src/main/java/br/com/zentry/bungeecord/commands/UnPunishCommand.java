package br.com.zentry.bungeecord.commands;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.player.ZentryPlayer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

@SuppressWarnings("deprecation")
public class UnPunishCommand extends BungeeCommandBase {
	
	public UnPunishCommand() {
		super("unpunish", "", "unp", "despunir");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!sender.hasPermission("zentry.bungee.command.unpunish")) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para remover puni��es.");
			return;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Utilize: /" + getName() + " <player>");
			return;
		}
		if(args.length == 1) {
			ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
			if(target == null) {
				UUID uuid = null;
				if((uuid = manager.getCachedUuid(args[0])) != null) {
					ZentryPlayer player = new ZentryPlayer(uuid, manager);
					if(!player.userExists()) {
						sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
						return;
					}
					if(!player.getPunishmentManager().isPunished()) {
						sender.sendMessage(ChatColor.RED + "Este jogador n�o est� punido.");
						return;
					}
					boolean ban = player.getPunishmentManager().isBan();
					player.getPunishmentManager().setBan(false); player.getPunishmentManager().setExpire(0);
					player.getPunishmentManager().setPunisher(null); player.getPunishmentManager().setReason(null);
					player.getPunishmentManager().setTemporary(false); player.getPunishmentManager().setTime(0);
					sender.sendMessage(ChatColor.GREEN + "Voc� des" + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ").");
					for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
						if(players.hasPermission("zentry.bungee.punishment")) players.sendMessage(ChatColor.RED + sender.getName() + " des" + (ban ? "baniu" : "mutou") + " " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.RED + ").");
					}
					return;
				} else if((uuid = manager.getOnlineUuid(args[0])) != null) {
					ZentryPlayer player = new ZentryPlayer(uuid, manager);
					if(!player.userExists()) {
						sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
						return;
					}
					if(!player.getPunishmentManager().isPunished()) {
						sender.sendMessage(ChatColor.RED + "Este jogador n�o est� punido.");
						return;
					}
					boolean ban = player.getPunishmentManager().isBan();
					player.getPunishmentManager().setBan(false); player.getPunishmentManager().setExpire(0);
					player.getPunishmentManager().setPunisher(null); player.getPunishmentManager().setReason(null);
					player.getPunishmentManager().setTemporary(false); player.getPunishmentManager().setTime(0);
					sender.sendMessage(ChatColor.GREEN + "Voc� des" + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ").");
					for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
						if(players.hasPermission("zentry.bungee.punishment")) players.sendMessage(ChatColor.RED + sender.getName() + " des" + (ban ? "baniu" : "mutou") + " " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.RED + ").");
					}
					return;
				} else if((uuid = manager.formatUuid(args[0])) != null) {
					ZentryPlayer player = new ZentryPlayer(uuid, manager);
					if(!player.userExists()) {
						sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
						return;
					}
					if(!player.getPunishmentManager().isPunished()) {
						sender.sendMessage(ChatColor.RED + "Este jogador n�o est� punido.");
						return;
					}
					boolean ban = player.getPunishmentManager().isBan();
					player.getPunishmentManager().setBan(false); player.getPunishmentManager().setExpire(0);
					player.getPunishmentManager().setPunisher(null); player.getPunishmentManager().setReason(null);
					player.getPunishmentManager().setTemporary(false); player.getPunishmentManager().setTime(0);
					sender.sendMessage(ChatColor.GREEN + "Voc� des" + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ").");
					for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
						if(players.hasPermission("zentry.bungee.punishment")) players.sendMessage(ChatColor.RED + sender.getName() + " des" + (ban ? "baniu" : "mutou") + " " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.RED + ").");
					}
					return;
				} else if((uuid = manager.getUuid(args[0])) != null) {
					ZentryPlayer player = new ZentryPlayer(uuid, manager);
					if(!player.userExists()) {
						sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
						return;
					}
					if(!player.getPunishmentManager().isPunished()) {
						sender.sendMessage(ChatColor.RED + "Este jogador n�o est� punido.");
						return;
					}
					boolean ban = player.getPunishmentManager().isBan();
					player.getPunishmentManager().setBan(false); player.getPunishmentManager().setExpire(0);
					player.getPunishmentManager().setPunisher(null); player.getPunishmentManager().setReason(null);
					player.getPunishmentManager().setTemporary(false); player.getPunishmentManager().setTime(0);
					sender.sendMessage(ChatColor.GREEN + "Voc� des" + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ").");
					for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
						if(players.hasPermission("zentry.bungee.punishment")) players.sendMessage(ChatColor.RED + sender.getName() + " des" + (ban ? "baniu" : "mutou") + " " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.RED + ").");
					}
					return;
				} else {
					sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar UUID do usu�rio solicitado.");
				}
				return;
			}
			ZentryPlayer player = new ZentryPlayer(target.getUniqueId(), manager);
			if(!player.userExists()) {
				sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
				return;
			}
			if(!player.getPunishmentManager().isPunished()) {
				sender.sendMessage(ChatColor.RED + "Este jogador n�o est� punido.");
				return;
			}
			boolean ban = player.getPunishmentManager().isBan();
			player.getPunishmentManager().setBan(false); player.getPunishmentManager().setExpire(0);
			player.getPunishmentManager().setPunisher(null); player.getPunishmentManager().setReason(null);
			player.getPunishmentManager().setTemporary(false); player.getPunishmentManager().setTime(0);
			if(!ban) player.sendMessage(ChatColor.YELLOW + "Voc� foi desmutado e agora pode enviar mensagens no chat.");
			sender.sendMessage(ChatColor.GREEN + "Voc� des" + (ban ? "baniu" : "mutou") + " o jogador " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.GREEN + ").");
			return;
		}
		if(args.length >= 2) {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return;
		}
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		if(args.length == 0 || args.length >= 2) {
			return Collections.emptyList();
		}
		List<String> matches = new ArrayList<>();
		if(args.length == 1) {
			List<String> identifiers = new ArrayList<>();
			ResultSet resultSet = null;
			try {
				resultSet = UtilsManager.getMysql().getStatement().executeQuery("SELECT * FROM `" + UtilsManager.getMysql().getDatabase() + "`.`player_punishment` WHERE `identifier`;");
				while(resultSet.next()) {
					identifiers.add(resultSet.getString("identifier"));
				}
			} catch(Exception exception) {
				exception.printStackTrace();
				return null;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
			for(String identifier : identifiers) {
				matches.add(new ZentryPlayer(manager.formatUuid(identifier), manager).getName());
			}
		}
		return matches;
	}
}

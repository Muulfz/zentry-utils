package br.com.zentry.bungeecord.commands;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.player.ZentryPlayer;
import br.com.zentry.manager.player.ZentryPlayer.DataManager;
import br.com.zentry.manager.player.ZentryPlayer.GroupManager;
import br.com.zentry.manager.player.ZentryPlayer.PunishmentManager;
import br.com.zentry.manager.player.ZentryPlayer.StatsManager;
import br.com.zentry.utils.enums.Groups;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

@SuppressWarnings("deprecation")
public class AccountCommand extends BungeeCommandBase {
	
	public AccountCommand() {
		super("account", "", "acc", "info");
	}
	
	public enum Modes {
		GROUPSET, GIVEPERMISSION, REMOVEPERMISSION;
	}
	
	public String information(ZentryPlayer player) {
		if(!player.userExists()) {
			StringBuilder builder = new StringBuilder(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
			return builder.toString().trim();
		}
		GroupManager group = player.getGroupManager();
		DataManager data = player.getDataManager();
		StatsManager stats = player.getStatsManager();
		PunishmentManager punishment = player.getPunishmentManager();
		if(punishment.isPunished() && punishment.isBan()) {
			StringBuilder builder = new StringBuilder(ChatColor.YELLOW + "Este jogador foi banido " + (punishment.isTemporary() ? "temporariamente" : "permanentemente") + ChatColor.YELLOW + " em " + ChatColor.WHITE + punishment.formatTime() + ChatColor.YELLOW + " por " + ChatColor.WHITE + punishment.getReason() + ChatColor.YELLOW + "." + (punishment.isTemporary() ? " Seu banimento expira em: " + ChatColor.WHITE + UtilsManager.formatLong(punishment.getExpire()) + ChatColor.YELLOW + "." : ""));
			return builder.toString().trim();
		}
		StringBuilder builder = new StringBuilder(ChatColor.YELLOW + "Informa��es de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + "):").append("\n");
		builder.append("" + ChatColor.YELLOW + ChatColor.STRIKETHROUGH + "---------------------------------").append("\n");
		builder.append(ChatColor.YELLOW + "  Grupo: " + ChatColor.WHITE + group.getGroup().name()).append("\n");
		builder.append(ChatColor.GRAY + "    Adicionado em " + group.formatLastChange() + " por " + group.getLastChanger()).append("\n");
		if(group.isTemporary()) builder.append(ChatColor.GRAY + "    Expira em: " + UtilsManager.formatLong(group.getExpire())).append("\n");
		builder.append(ChatColor.YELLOW + "  Rank: " + stats.getRank().getColor() + stats.getRank().name()).append("\n");
		builder.append(ChatColor.YELLOW + "  Primeiro login: " + ChatColor.WHITE + data.formatFirstLogin()).append("\n");
		builder.append(ChatColor.YELLOW + "  Ultimo login: " + ChatColor.WHITE + data.formatLastLogin()).append("\n");
		builder.append(ChatColor.YELLOW + "  Visto por �ltimo: " + ChatColor.WHITE + (data.getLastSeen() == 0 ? "Online" : data.formatLastSeen())).append("\n");
		if(punishment.isPunished() && !punishment.isBan()) builder.append(ChatColor.GRAY + "  Foi mutado " + (punishment.isTemporary() ? "temporariamente" : "permanentemente") + " em " + punishment.formatTime() + " por " + punishment.getReason() + ".").append("\n");
		builder.append("" + ChatColor.YELLOW + ChatColor.STRIKETHROUGH + "---------------------------------");
		return builder.toString().trim();
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer && !((ProxiedPlayer)sender).hasPermission(getPermission())) {
			sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para executar este comando.");
			return;
		}
		if(args.length == 0) {
			if(!(sender instanceof ProxiedPlayer)) {
				sender.sendMessage(ChatColor.RED + "Voc� n�o possui informa��es para serem exibidas.");
				return;
			}
			sender.sendMessage(information(new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager)));
			return;
		}
		if(args.length == 1) {
			if(!sender.hasPermission("zentry.bungee.command.account")) {
				sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para visualizar a conta de outros usu�rios.");
				return;
			}
			ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
			if(target == null) {
				UUID uuid = null;
				if((uuid = manager.getCachedUuid(args[0])) != null) {
					sender.sendMessage(information(new ZentryPlayer(uuid, manager)));
					return;
				} else if((uuid = manager.getOnlineUuid(args[0])) != null) {
					sender.sendMessage(information(new ZentryPlayer(uuid, manager)));
					return;
				} else if((uuid = manager.formatUuid(args[0])) != null) {
					sender.sendMessage(information(new ZentryPlayer(uuid, manager)));
					return;
				} else if((uuid = manager.getUuid(args[0])) != null) {
					sender.sendMessage(information(new ZentryPlayer(uuid, manager)));
					return;
				} else {
					sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar UUID do usu�rio solicitado.");
				}
				return;
			}
			sender.sendMessage(information(new ZentryPlayer(target.getUniqueId(), manager)));
			return;
		}
		if(args.length == 2) {
			if(!sender.hasPermission("zentry.bungee.command.account")) {
				sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para manejar a conta de outros usu�rios.");
				return;
			}
			Modes mode = null;
			try { mode = Modes.valueOf(args[1].toUpperCase()); } catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o modo citado.");
			}
			switch(mode) {
			case GROUPSET:
				sender.sendMessage(ChatColor.RED + "Utilize: /" + getName() + " " + args[0] + " " + Modes.GROUPSET.name().toLowerCase() + " <group> [time]");
				break;
			default:
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar uma a��o para a op��o escolhida.");
				break;
			}
			return;
		}
		if(args.length == 3) {
			if(!sender.hasPermission("zentry.bungee.command.account")) {
				sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para manejar a conta de outros usu�rios.");
				return;
			}
			ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
			Modes mode = null;
			try { mode = Modes.valueOf(args[1].toUpperCase()); } catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o modo citado.");
			}
			if(target == null) {
				UUID uuid = null;
				switch(mode) {
				case GROUPSET:
					Groups group = null;
					try { group = Groups.valueOf(args[2].toUpperCase().toUpperCase()); } catch(Exception exception) {
						sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o grupo citado.");
						return;
					}
					if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < group.ordinal()) {
						sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar este grupo.");
						return;
					}
					if((uuid = manager.getCachedUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
							sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
							return;
						}
						if(player.getGroupManager().getGroup().equals(group)) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
							return;
						}
						player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(0);
						player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());
						sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " com sucesso!");
						return;
					} else if((uuid = manager.getOnlineUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
							sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
							return;
						}
						if(player.getGroupManager().getGroup().equals(group)) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
							return;
						}
						player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(0);
						player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());							sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " com sucesso!");
						return;
					} else if((uuid = manager.formatUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
							sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
							return;
						}
						if(player.getGroupManager().getGroup().equals(group)) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
							return;
						}
						player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(0);
						player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());							sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " com sucesso!");
						return;
					} else if((uuid = manager.getUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
							sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
							return;
						}
						if(player.getGroupManager().getGroup().equals(group)) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
							return;
						}
						player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(0);
						player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());							sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " com sucesso!");
						return;
					} else {
						sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar UUID do usu�rio solicitado.");
						return;
					}
				case GIVEPERMISSION:
					if(!sender.hasPermission("zentry.bungee.command.account.permissions")) {
						sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para manejar permiss�es.");
						return;
					}
					if((uuid = manager.getCachedUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(player.getGroupManager().hasPermission(args[2])) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui esta permiss�o.");
							return;
						}
						player.getGroupManager().setPermission(args[2], true);
						sender.sendMessage(ChatColor.GREEN + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.GREEN + "\" adicionada com sucesso.");
						return;
					} else if((uuid = manager.getOnlineUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(player.getGroupManager().hasPermission(args[2])) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui esta permiss�o.");
							return;
						}
						player.getGroupManager().setPermission(args[2], true);
						sender.sendMessage(ChatColor.GREEN + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.GREEN + "\" adicionada com sucesso.");
						return;
					} else if((uuid = manager.formatUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(player.getGroupManager().hasPermission(args[2])) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui esta permiss�o.");
							return;
						}
						player.getGroupManager().setPermission(args[2], true);
						sender.sendMessage(ChatColor.GREEN + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.GREEN + "\" adicionada com sucesso.");
						return;
					} else if((uuid = manager.getUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(player.getGroupManager().hasPermission(args[2])) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui esta permiss�o.");
							return;
						}
						player.getGroupManager().setPermission(args[2], true);
						sender.sendMessage(ChatColor.GREEN + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.GREEN + "\" adicionada com sucesso.");
						return;
					} else {
						sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar UUID do usu�rio solicitado.");
						return;
					}
				case REMOVEPERMISSION:
					if(!sender.hasPermission("zentry.bungee.command.account.permissions")) {
						sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para manejar permiss�es.");
						return;
					}
					if((uuid = manager.getCachedUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(!player.getGroupManager().hasPermission(args[2])) {
							sender.sendMessage(ChatColor.RED + "Este jogador n�o possui esta permiss�o.");
							return;
						}
						player.getGroupManager().setPermission(args[2], false);
						sender.sendMessage(ChatColor.RED + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.RED + "\" removida com sucesso.");
						return;
					} else if((uuid = manager.getOnlineUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(!player.getGroupManager().hasPermission(args[2])) {
							sender.sendMessage(ChatColor.RED + "Este jogador n�o possui esta permiss�o.");
							return;
						}
						player.getGroupManager().setPermission(args[2], false);
						sender.sendMessage(ChatColor.RED + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.RED + "\" removida com sucesso.");
						return;
					} else if((uuid = manager.formatUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(!player.getGroupManager().hasPermission(args[2])) {
							sender.sendMessage(ChatColor.RED + "Este jogador n�o possui esta permiss�o.");
							return;
						}
						player.getGroupManager().setPermission(args[2], false);
						sender.sendMessage(ChatColor.RED + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.RED + "\" removida com sucesso.");
						return;
					} else if((uuid = manager.getUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(!player.getGroupManager().hasPermission(args[2])) {
							sender.sendMessage(ChatColor.RED + "Este jogador n�o possui esta permiss�o.");
							return;
						}
						player.getGroupManager().setPermission(args[2], false);
						sender.sendMessage(ChatColor.RED + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.RED + "\" removida com sucesso.");
						return;
					} else {
						sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar UUID do usu�rio solicitado.");
						return;
					}
				default:
					sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar uma a��o para a op��o escolhida.");
					break;
				}
				return;
			}
			ZentryPlayer player = new ZentryPlayer(target.getUniqueId(), manager);
			switch(mode) {
			case GROUPSET:
				Groups group = null;
				try { group = Groups.valueOf(args[2].toUpperCase().toUpperCase()); } catch(Exception exception) {
					sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o grupo citado.");
					return;
				}
				if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < group.ordinal()) {
					sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar este grupo.");
					return;
				}
				if(!player.userExists()) {
					sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
					return;
				}
				if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
					sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
					return;
				}
				if(player.getGroupManager().getGroup().equals(group)) {
					sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
					return;
				}
				player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(0);
				player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());
				sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " com sucesso!");
				player.getProxiedPlayer().disconnect(ChatColor.GREEN + "Seu grupo foi alterado para " + ChatColor.WHITE + group.name() + ChatColor.GREEN + "!\n" + ChatColor.YELLOW + "Relogue para receber suas permiss�es.");
				return;
			case GIVEPERMISSION:
				if(!sender.hasPermission("zentry.bungee.command.account.permissions")) {
					sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para manejar permiss�es.");
					return;
				}
				if(!player.userExists()) {
					sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
					return;
				}
				if(player.getGroupManager().hasPermission(args[2])) {
					sender.sendMessage(ChatColor.RED + "Este jogador j� possui esta permiss�o.");
					return;
				}
				player.getGroupManager().setPermission(args[2], true);
				sender.sendMessage(ChatColor.GREEN + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.GREEN + "\" adicionada com sucesso.");
				player.getProxiedPlayer().disconnect(ChatColor.GREEN + "A permiss�o " + ChatColor.WHITE + args[2] + ChatColor.GREEN + " foi cedida a seu usu�rio.\n" + ChatColor.YELLOW + "Relogue para receb�-la.");
				return;
			case REMOVEPERMISSION:
				if(!sender.hasPermission("zentry.bungee.command.account.permissions")) {
					sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para manejar permiss�es.");
					return;
				}
				if(!player.userExists()) {
					sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
					return;
				}
				if(!player.getGroupManager().hasPermission(args[2])) {
					sender.sendMessage(ChatColor.RED + "Este jogador n�o possui esta permiss�o.");
					return;
				}
				player.getGroupManager().setPermission(args[2], false);
				sender.sendMessage(ChatColor.RED + "Permiss�o \"" + ChatColor.WHITE + args[2] + ChatColor.RED + "\" removida com sucesso.");
				player.getProxiedPlayer().disconnect(ChatColor.GREEN + "A permiss�o " + ChatColor.WHITE + args[2] + ChatColor.GREEN + " foi cedida a seu usu�rio.\n" + ChatColor.YELLOW + "Relogue para receb�-la.");
				return;
			default:
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar uma a��o para a op��o escolhida.");
				break;
			}
			return;
		}
		if(args.length == 4) {
			if(!sender.hasPermission("zentry.bungee.command.account")) {
				sender.sendMessage(ChatColor.RED + "Voc� n�o possui permiss�o para manejar a conta de outros usu�rios.");
				return;
			}
			ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
			Modes mode = null;
			try { mode = Modes.valueOf(args[1].toUpperCase()); } catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o modo citado.");
			}
			Groups group = null;
			try { group = Groups.valueOf(args[2].toUpperCase().toUpperCase()); } catch(Exception exception) {
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar o grupo citado.");
				return;
			}
			if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < group.ordinal()) {
				sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar este grupo.");
				return;
			}
			long time = UtilsManager.stringToLong(args[3]);
			if(time <= 0) {
				sender.sendMessage(ChatColor.RED + "Formato incorreto. Utilize: 1+d,1+h,1+m,1+s");
				return;
			}
			if(target == null) {
				UUID uuid = null;
				switch(mode) {
				case GROUPSET:
					if((uuid = manager.getCachedUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
							sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
							return;
						}
						if(player.getGroupManager().getGroup().equals(group)) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
							return;
						}
						player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(time);
						player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());
						sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " por " + ChatColor.WHITE + UtilsManager.formatLong(time) + ChatColor.YELLOW + " com sucesso!");
						return;
					} else if((uuid = manager.getOnlineUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
							sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
							return;
						}
						if(player.getGroupManager().getGroup().equals(group)) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
							return;
						}
						player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(time);
						player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());
						sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " por " + ChatColor.WHITE + UtilsManager.formatLong(time) + ChatColor.YELLOW + " com sucesso!");
						return;
					} else if((uuid = manager.formatUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
							sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
							return;
						}
						if(player.getGroupManager().getGroup().equals(group)) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
							return;
						}
						player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(time);
						player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());
						sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " por " + ChatColor.WHITE + UtilsManager.formatLong(time) + ChatColor.YELLOW + " com sucesso!");
						return;
					} else if((uuid = manager.getUuid(args[0])) != null) {
						ZentryPlayer player = new ZentryPlayer(uuid, manager);
						if(!player.userExists()) {
							sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
							return;
						}
						if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
							sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
							return;
						}
						if(player.getGroupManager().getGroup().equals(group)) {
							sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
							return;
						}
						player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(time);
						player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());
						sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " por " + ChatColor.WHITE + UtilsManager.formatLong(time) + ChatColor.YELLOW + " com sucesso!");
						return;
					} else {
						sender.sendMessage(ChatColor.RED + "N�o foi poss�vel retornar UUID do usu�rio solicitado.");
						return;
					}
				default:
					sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar uma a��o para a op��o escolhida.");
					break;
				}
				return;
			}
			ZentryPlayer player = new ZentryPlayer(target.getUniqueId(), manager);
			switch(mode) {
			case GROUPSET:
				if(!player.userExists()) {
					sender.sendMessage(ChatColor.RED + "Usu�rio parcial ou completamente inexistente.");
					return;
				}
				if(sender instanceof ProxiedPlayer && new ZentryPlayer(((ProxiedPlayer)sender).getUniqueId(), manager).getGroupManager().getGroup().ordinal() < player.getGroupManager().getGroup().ordinal()) {
					sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
					return;
				}
				if(player.getGroupManager().getGroup().equals(group)) {
					sender.sendMessage(ChatColor.RED + "Este jogador j� possui este grupo");
					return;
				}
				player.getGroupManager().setGroup(group); player.getGroupManager().setExpire(time);
				player.getGroupManager().setLastChange(new Timestamp(System.currentTimeMillis()).getTime()); player.getGroupManager().setLastChanger(sender.getName());
				sender.sendMessage(ChatColor.YELLOW + "Voc� alterou o grupo de " + player.getName() + "(" + ChatColor.WHITE + player.getIdentifier() + ChatColor.YELLOW + ") para " + ChatColor.WHITE + group.name() + ChatColor.YELLOW + " por " + ChatColor.WHITE + UtilsManager.formatLong(time) + ChatColor.YELLOW + " com sucesso!");
				player.getProxiedPlayer().disconnect(ChatColor.GREEN + "Seu grupo foi alterado para " + ChatColor.WHITE + group.name() + ChatColor.GREEN + " por " + ChatColor.WHITE + UtilsManager.formatLong(time) + ChatColor.GREEN + "!\n" + ChatColor.YELLOW + "Relogue para receber suas permiss�es.");
				return;
			case GIVEPERMISSION:
				sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
				break;
			case REMOVEPERMISSION:
				sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
				break;
			default:
				sender.sendMessage(ChatColor.RED + "N�o foi poss�vel identificar uma a��o para a op��o escolhida.");
				break;
			}
			return;
		}
		if(args.length == 5) {
			sender.sendMessage(ChatColor.RED + "Voc� utilizou argumentos que excedem o limite do comando.");
			return;
		}
	}
	
	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		if(args.length == 0 || args.length >= 5 || !sender.hasPermission("zentry.bungee.command.account")) {
			return Collections.emptyList();
		}
		List<String> matches = new ArrayList<>();
		if(args.length == 1) {
			for(ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
				if(player.getName().toLowerCase().startsWith(args[0].toLowerCase())) matches.add(player.getName());
			}
		}
		if(args.length == 2) {
			for(Modes modes : Modes.values()) {
				if(ProxyServer.getInstance().getPlayer(args[0]) == null) {
					sender.sendMessage(ChatColor.RED + "O jogador citado est� offline.");
					return Collections.emptyList();
				}
				if(modes.name().toLowerCase().startsWith(args[1].toLowerCase())) {
					if(sender instanceof ProxiedPlayer) {
						if(new ZentryPlayer(manager.getCachedUuid(sender.getName()), manager).getGroupManager().getGroup().ordinal() < new ZentryPlayer(manager.getCachedUuid(args[0]), manager).getGroupManager().getGroup().ordinal()) {
							sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar a conta deste jogador.");
							return Collections.emptyList();
						} else matches.add(modes.name().toLowerCase());
					} else {
						matches.add(modes.name().toLowerCase());
					}
				}
			}
		}
		if(args.length == 3) {
			if(args[1].equalsIgnoreCase(Modes.GROUPSET.name())) {
				for(Groups groups : Groups.values()) {
					if(groups.name().toLowerCase().startsWith(args[2].toLowerCase())) {
						if(ProxyServer.getInstance().getPlayer(args[0]) == null) {
							sender.sendMessage(ChatColor.RED + "O jogador citado est� offline.");
							return Collections.emptyList();
						}
						if(sender instanceof ProxiedPlayer) {
							if(new ZentryPlayer(manager.getCachedUuid(sender.getName()), manager).getGroupManager().getGroup().ordinal() < new ZentryPlayer(manager.getCachedUuid(args[0]), manager).getGroupManager().getGroup().ordinal()) {
								sender.sendMessage(ChatColor.RED + "Voc� n�o pode manejar o grupo deste jogador.");
								return Collections.emptyList();
							}
							else if(new ZentryPlayer(manager.getCachedUuid(sender.getName()), manager).getGroupManager().getGroup().ordinal() >= groups.ordinal()) matches.add(groups.name().toLowerCase());
						} else {
							matches.add(groups.name().toLowerCase());
						}
					}
				}
			} else {
				return Collections.emptyList();
			}
		}
		if(args.length == 4) matches = new LinkedList<>(Arrays.asList(new String[]{"n", "1d", "1h", "1m", "1s"}));
		return matches;
	}
}

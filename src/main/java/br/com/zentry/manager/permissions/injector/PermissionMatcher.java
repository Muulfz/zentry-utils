package br.com.zentry.manager.permissions.injector;

public interface PermissionMatcher {
    boolean isMatches(String p0, String p1);
}

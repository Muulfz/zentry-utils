package br.com.zentry.manager.youtuber;

public class Channel {
	
	private String name;
	private String description;
	private int subscribers;
	private int videos;
	private int views;
	private boolean hiddenSubscribers;
	
	public Channel(String name, String description, int subscribers, int videos, int views, boolean hiddenSubscribers) {
		this.name = name;
		this.description = description;
		this.subscribers = subscribers;
		this.videos = videos;
		this.views = views;
		this.hiddenSubscribers = hiddenSubscribers;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public int getSubscribers() {
		return subscribers;
	}
	
	public int getVideos() {
		return videos;
	}
	
	public int getViews() {
		return views;
	}
	
	public boolean hiddenSubscribers() {
		return hiddenSubscribers;
	}
}

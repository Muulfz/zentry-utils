package br.com.zentry.manager.youtuber;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import br.com.zentry.manager.UtilsManager;

public class YoutuberManager {
	
	public static List<Youtuber> getYoutubers() {
		List<Youtuber> list = new ArrayList<>();
		ResultSet resultSet = null;
		try {
			resultSet = UtilsManager.getMysql().getStatement().executeQuery("SELECT * FROM `" + UtilsManager.getMysql().getDatabase() + "`.`global_youtubers` WHERE `uuid`;");
			while(resultSet.next()) {
				UUID uuid = UUID.fromString(resultSet.getString("uuid")); String channel = resultSet.getString("channel"); long time = resultSet.getLong("time");
				Youtuber youtuber = new Youtuber(uuid, channel, time);
				list.add(youtuber);
			}
			return list;
		} catch(Exception exception) {
			exception.printStackTrace();
		} finally {
			try {
				resultSet.close();
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return Collections.emptyList();
	}
	
	public static List<Youtuber> getYoutubersByUniqueId(UUID uuid) {
		List<Youtuber> list = new ArrayList<>();
		for(Youtuber youtuber : getYoutubers()) {
			if(youtuber.getUuid().toString().equals(uuid.toString())) list.add(youtuber);
		}
		return list;
	}
	
	public static boolean registerApplication(Youtuber youtuber) {
		try {
			UtilsManager.getMysql().getStatement().execute("INSERT INTO `" + UtilsManager.getMysql().getDatabase() + "`.`global_youtubers` (`uuid`, `channel`, `time`) VALUES ('" + youtuber.getUuid().toString() + "', '" + youtuber.getChannel() + "', '" + youtuber.getTime() + "');");
			return true;
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return false;
	}
	
	public static boolean unregisterApplication(Youtuber youtuber) {
		ResultSet resultSet = null;
		try {
			resultSet = UtilsManager.getMysql().getStatement().executeQuery("SELECT * FROM `" + UtilsManager.getMysql().getDatabase() + "`.`global_youtubers` WHERE `uuid` = '" + youtuber.getUuid().toString() + "';");
			if(!resultSet.next()) return false;
			UtilsManager.getMysql().getStatement().execute("DELETE FROM `" + UtilsManager.getMysql().getDatabase() + "`.`global_youtubers` WHERE `uuid` = '" + youtuber.getUuid().toString() + "';");
			return true;
		} catch(Exception exception) {
			exception.printStackTrace();
		} finally {
			try {
				resultSet.close();
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return false;
	}
}

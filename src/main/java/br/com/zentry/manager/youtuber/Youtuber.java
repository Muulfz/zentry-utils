package br.com.zentry.manager.youtuber;

import java.util.UUID;

public class Youtuber {
	
	private UUID uuid;
	private String channel;
	private long time;
	
	public Youtuber(UUID uuid, String channel, long time) {
		this.uuid = uuid;
		this.channel = channel;
		this.time = time;
	}
	
	public UUID getUuid() {
		return uuid;
	}
	
	public String getChannel() {
		return channel;
	}
	
	public long getTime() {
		return time;
	}
}

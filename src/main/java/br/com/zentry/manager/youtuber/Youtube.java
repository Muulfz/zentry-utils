package br.com.zentry.manager.youtuber;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Youtube {
	
	private static final String base(String part, String channel) {
		return "https://www.googleapis.com/youtube/v3/channels?part=" + part + "&id=" + channel + "&key=AIzaSyBagbS01Sxrr-2tUdhU4AXvsI41p1OUF0A";
	}
	
	public static Channel getChannel(String id) throws NumberFormatException, IOException {
		String name = null, description = null; int views = 0, subscribers = 0, videos = 0; boolean hiddenSubscribers = false;
		JsonParser parser = new JsonParser();
		//Statistics
		HttpURLConnection statisticsConnection = (HttpURLConnection) new URL(base("statistics", id)).openConnection();
		JsonElement statisticsElement = parser.parse(new InputStreamReader((InputStream)statisticsConnection.getContent()));
		JsonObject statistics = statisticsElement.getAsJsonObject().get("items").getAsJsonArray().get(0).getAsJsonObject().get("statistics").getAsJsonObject();
		views = statistics.get("viewCount").getAsInt();
		subscribers = statistics.get("subscriberCount").getAsInt();
		videos = statistics.get("videoCount").getAsInt();
		hiddenSubscribers = statistics.get("hiddenSubscriberCount").getAsBoolean();
		//Snippet
		HttpURLConnection snippetConnection = (HttpURLConnection) new URL(base("snippet", id)).openConnection();
		JsonElement snippetElement = parser.parse(new InputStreamReader((InputStream)snippetConnection.getContent()));
		JsonObject snippet = snippetElement.getAsJsonObject().get("items").getAsJsonArray().get(0).getAsJsonObject().get("snippet").getAsJsonObject();
		name = snippet.get("title").getAsString();
		description = snippet.get("description").getAsString();
		return new Channel(name, description, subscribers, videos, views, hiddenSubscribers);
	}
	
	public static boolean channelExists(String channel) throws Exception {
		HttpURLConnection connection = (HttpURLConnection) new URL(base("statistics", channel)).openConnection();
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(new InputStreamReader((InputStream)connection.getContent()));
		JsonObject object = element.getAsJsonObject();
		return object.get("pageInfo").getAsJsonObject().get("totalResults").getAsInt() > 0 && object.get("items").getAsJsonArray().size() > 0;
	}
	
	public static JsonObject getVideoStatistics(String video) throws Exception {
		HttpURLConnection connection = (HttpURLConnection) new URL("https://www.googleapis.com/youtube/v3/videos?key=AIzaSyBagbS01Sxrr-2tUdhU4AXvsI41p1OUF0A&part=statistics&id=" + video).openConnection();
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(new InputStreamReader((InputStream)connection.getContent()));
		return element.getAsJsonObject().get("items").getAsJsonArray().get(0).getAsJsonObject().get("statistics").getAsJsonObject();
	}
	
	public static List<Video> getVideosByChannel(String channel) throws NumberFormatException, Exception {
		String title, description; int views, comments, likes, deslikes;
		List<Video> list = new ArrayList<>();
		HttpURLConnection connection = (HttpURLConnection) new URL("https://www.googleapis.com/youtube/v3/search?key=AIzaSyBagbS01Sxrr-2tUdhU4AXvsI41p1OUF0A&channelId=UCgUyTs3KXHNjsS3FO7hlekQ&part=snippet,id&order=date").openConnection();
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(new InputStreamReader((InputStream)connection.getContent()));
		JsonArray array = element.getAsJsonObject().get("items").getAsJsonArray();
		for(int i = 0; i < array.size(); i++) {
			JsonObject object = array.get(i).getAsJsonObject().get("snippet").getAsJsonObject();
			title = object.get("title").getAsString();
			description = object.get("description").getAsString();
			JsonObject identificator = element.getAsJsonObject().get("items").getAsJsonArray().get(i).getAsJsonObject().get("id").getAsJsonObject();
			if(identificator.has("channelId")) break;
			JsonObject statistics = getVideoStatistics(element.getAsJsonObject().get("items").getAsJsonArray().get(i).getAsJsonObject().get("id").getAsJsonObject().get("videoId").getAsString());
			views = statistics.get("viewCount").getAsInt();
			comments = statistics.get("commentCount").getAsInt();
			likes = statistics.get("likeCount").getAsInt();
			deslikes = statistics.get("dislikeCount").getAsInt();
			Video video = new Video(title, description, views, comments, likes, deslikes);
			list.add(video);
		}
		return list;
	}
}

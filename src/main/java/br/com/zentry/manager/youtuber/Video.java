package br.com.zentry.manager.youtuber;

public class Video {
	
	private String title;
	private String description;
	private int views;
	private int comments;
	private int likes;
	private int deslikes;
	
	public Video(String title, String description, int views, int comments, int likes, int deslikes) {
		this.title = title;
		this.description = description;
		this.views = views;
		this.comments = comments;
		this.likes = likes;
		this.deslikes = deslikes;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public int getViews() {
		return views;
	}
	
	public int getComments() {
		return comments;
	}
	
	public int getLikes() {
		return likes;
	}
	
	public int getDeslikes() {
		return deslikes;
	}
}

package br.com.zentry.manager.player;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.connection.Mysql;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Authenticator {
	
	public static ArrayList<String> lock = new ArrayList<>();
	public static ArrayList<String> password = new ArrayList<>();
	
	private Mysql mysql = UtilsManager.getMysql();
	
	private UtilsManager manager;
	private UUID uuid;
	
	public Authenticator(UtilsManager manager, UUID uuid) {
		this.manager = manager; this.uuid = uuid;
	}
	
	@SuppressWarnings("deprecation")
	public boolean dataExists() {
		ResultSet resultSet = null;
		try {
			resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`global_authenticator` WHERE `uuid` = '" + uuid.toString() + "';");
			if(resultSet.next()) return true;
			return false;
		} catch(Exception exception) {
			if(manager.isBungeecord()) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
				if(player != null) player.disconnect(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			} else {
				Player player = Bukkit.getServer().getPlayer(uuid);
				if(player != null) player.kickPlayer(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			}
		} finally {
			try {
				resultSet.close();
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	public void createData() {
		if(dataExists()) return;
		GoogleAuthenticator authenticator = new GoogleAuthenticator();
		GoogleAuthenticatorKey key = authenticator.createCredentials();
		try {
			mysql.getStatement().execute("INSERT INTO `" + mysql.getDatabase() + "`.`global_authenticator` (`uuid`, `key`, `password`, `registered`, `logged`) VALUES ('" + uuid.toString() + "', '" + Base64.getEncoder().encodeToString(key.getKey().getBytes()) + "', '" + null + "', '" + 0  + "', '" + 0 + "');");
		} catch(Exception exception) {
			if(manager.isBungeecord()) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
				if(player != null) player.disconnect(ChatColor.RED + "N�o foi poss�vel inserir informa��es sobre seu autenticador.");
			} else {
				Player player = Bukkit.getServer().getPlayer(uuid);
				if(player != null) player.kickPlayer(ChatColor.RED + "N�o foi poss�vel inserir informa��es sobre seu autenticador.");
			}
		}
		return;
	}
	
	@SuppressWarnings("deprecation")
	public void deleteData() {
		if(!dataExists()) return;
		try {
			mysql.getStatement().execute("DELETE FROM `" + mysql.getDatabase() + "`.`global_authenticator` WHERE `id` AND `uuid` = '" + uuid.toString() + "';");
		} catch(Exception exception) {
			if(manager.isBungeecord()) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
				if(player != null) player.disconnect(ChatColor.RED + "N�o foi poss�vel inserir informa��es sobre seu autenticador.");
			} else {
				Player player = Bukkit.getServer().getPlayer(uuid);
				if(player != null) player.kickPlayer(ChatColor.RED + "N�o foi poss�vel inserir informa��es sobre seu autenticador.");
			}
		}
		return;
	}
	
	@SuppressWarnings("deprecation")
	public String getKey() {
		ResultSet resultSet = null;
		try {
			resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`global_authenticator` WHERE `uuid` = '" + uuid.toString() + "';");
			if(resultSet.next()) {
				return new String(Base64.getDecoder().decode(resultSet.getString("key")));
			}
			return null;
		} catch(Exception exception) {
			if(manager.isBungeecord()) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
				if(player != null) player.disconnect(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			} else {
				Player player = Bukkit.getServer().getPlayer(uuid);
				if(player != null) player.kickPlayer(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			}
		} finally {
			try {
				resultSet.close();
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public String getPassword() {
		ResultSet resultSet = null;
		try {
			resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`global_authenticator` WHERE `uuid` = '" + uuid.toString() + "';");
			if(resultSet.next()) {
				return resultSet.getString("password");
			}
			return null;
		} catch(Exception exception) {
			if(manager.isBungeecord()) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
				if(player != null) player.disconnect(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			} else {
				Player player = Bukkit.getServer().getPlayer(uuid);
				if(player != null) player.kickPlayer(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			}
		} finally {
			try {
				resultSet.close();
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public boolean hasRegistered() {
		ResultSet resultSet = null;
		try {
			resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`global_authenticator` WHERE `uuid` = '" + uuid.toString() + "';");
			if(resultSet.next()) {
				return resultSet.getInt("registered") == 1;
			}
			return false;
		} catch(Exception exception) {
			if(manager.isBungeecord()) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
				if(player != null) player.disconnect(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			} else {
				Player player = Bukkit.getServer().getPlayer(uuid);
				if(player != null) player.kickPlayer(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			}
		} finally {
			try {
				resultSet.close();
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	public boolean isLogged() {
		ResultSet resultSet = null;
		try {
			resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`global_authenticator` WHERE `uuid` = '" + uuid.toString() + "';");
			if(resultSet.next()) {
				return resultSet.getInt("logged") == 1;
			}
			return false;
		} catch(Exception exception) {
			if(manager.isBungeecord()) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
				if(player != null) player.disconnect(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			} else {
				Player player = Bukkit.getServer().getPlayer(uuid);
				if(player != null) player.kickPlayer(ChatColor.RED + "N�o foi poss�vel retornar informa��es sobre seu autenticador.");
			}
		} finally {
			try {
				resultSet.close();
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	private void set(String column, Object value) {
		if(!dataExists()) return;
		try {
			mysql.getStatement().execute("UPDATE `" + mysql.getDatabase() + "`.`global_authenticator` SET `" + column.toLowerCase() + "` = '" + value.toString() + "' WHERE `uuid` = '" + uuid.toString() + "';");
		} catch(Exception exception) {
			if(manager.isBungeecord()) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
				if(player != null) player.disconnect(ChatColor.RED + "N�o foi poss�vel inserir informa��es sobre seu autenticador.");
			} else {
				Player player = Bukkit.getServer().getPlayer(uuid);
				if(player != null) player.kickPlayer(ChatColor.RED + "N�o foi poss�vel inserir informa��es sobre seu autenticador.");
			}
		}
		return;
	}
	
	public void setKey(String key) {
		set("key", key);
		return;
	}
	
	public void setPassword(String password) {
		set("password", password);
		return;
	}
	
	public void setRegistered(boolean registered) {
		set("registered", registered ? 1 : 0);
		return;
	}
	
	public void setLogged(boolean logged) {
		set("logged", logged ? 1 : 0);
	}
}

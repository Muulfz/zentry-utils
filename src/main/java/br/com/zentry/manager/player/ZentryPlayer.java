package br.com.zentry.manager.player;

import java.net.InetSocketAddress;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.manager.connection.Mysql;
import br.com.zentry.spigot.ZentryUtils;
import br.com.zentry.utils.enums.Groups;
import br.com.zentry.utils.enums.Ranks;
import br.com.zentry.utils.enums.Tags;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ZentryPlayer {
	
	private UUID uuid;
	private boolean bungeecord;
	
	public ZentryPlayer(UUID uuid, UtilsManager manager) {
		this.uuid = uuid;
		bungeecord = manager.isBungeecord();
	}
	
	public UUID getUuid() {
		return uuid;
	}
	
	public boolean isOnline() {
		return bungeecord ? ProxyServer.getInstance().getPlayer(uuid) != null : Bukkit.getServer().getPlayer(getDataManager().getUsername()) != null;
	}
	
	public Player getPlayer() {
		return Bukkit.getServer().getPlayer(getDataManager().getUsername());
	}
	
	@SuppressWarnings("deprecation")
	public OfflinePlayer getOfflinePlayer() {
		return Bukkit.getServer().getOfflinePlayer(getDataManager().getUsername());
	}
	
	public ProxiedPlayer getProxiedPlayer() {
		return ProxyServer.getInstance().getPlayer(uuid);
	}
	
	public String getName() {
		return bungeecord ? isOnline() ? getProxiedPlayer().getName() : getDataManager().getUsername() : isOnline() ? getPlayer().getName() : getOfflinePlayer().getName();
	}
	
	public String getAddress() {
		return bungeecord ? isOnline() ? getProxiedPlayer().getAddress().toString().replace("/", "").replace(":", "-") : null : isOnline() ? getPlayer().getAddress().toString().replace("/", "").replace(":", "-") : null;
	}
	
	public String getIdentifier() {
		return uuid.toString().replace("-", "");
	}
	
	public void sendMessage(Object message) {
		if(!isOnline()) return;
		if(bungeecord) getProxiedPlayer().sendMessage(new BaseComponent[]{new TextComponent(String.valueOf(message))});
		else getPlayer().sendMessage(String.valueOf(message));
	}
	
	public void kick(String reason) {
		if(!isOnline()) return;
		if(bungeecord) getProxiedPlayer().disconnect(new BaseComponent[]{new TextComponent(ChatColor.RED + reason)});
		else getPlayer().kickPlayer(ChatColor.RED + reason);
	}
	
	public boolean hasPermission(String permission) {
		if(bungeecord) {
			return getProxiedPlayer().hasPermission(permission);
		}
		return getPlayer() != null ? getPlayer().hasPermission(permission) : new LinkedList<String>(Arrays.asList(getGroupManager().getGroup().getPermissions())).contains(permission) || getGroupManager().hasPermission(permission);
	}
	
	public DataManager getDataManager() {
		return new DataManager(this);
	}
	
	public GroupManager getGroupManager() {
		return new GroupManager(this);
	}
	
	public StatsManager getStatsManager() {
		return new StatsManager(this);
	}
	
	public PunishmentManager getPunishmentManager() {
		return new PunishmentManager(this);
	}
	
	public TagManager getTagManager() {
		return new TagManager(this);
	}
	
	public void createUser() {
		getDataManager().createData();
		getGroupManager().createData();
		getStatsManager().createData();
		getPunishmentManager().createData();
	}
	
	public void deleteUser() {
		getDataManager().deleteData();
		getGroupManager().deleteData();
		getStatsManager().deleteData();
		getPunishmentManager().deleteData();
	}
	
	public boolean userExists() {
		return getDataManager().dataExists() && getGroupManager().dataExists() && getStatsManager().dataExists() && getPunishmentManager().dataExists();
	}
	
	public void injectPermissions() {
		if(bungeecord) {
			for(String permission : getGroupManager().getGroup().getPermissions()) getProxiedPlayer().setPermission(permission, true);
			for(String permission : getGroupManager().getPermissions()) getProxiedPlayer().setPermission(permission, true);
			return;
		}
		PermissionAttachment attachment = getPlayer().addAttachment(ZentryUtils.getPlugin());
		for(String permission : getGroupManager().getGroup().getPermissions()) attachment.setPermission(permission, true);
		for(String permission : getGroupManager().getPermissions()) attachment.setPermission(permission, true);
		return;
	}
	
	public void unloadPermissions() {
		if(bungeecord) {
			List<String> list = new ArrayList<>();
			for(String permission : getProxiedPlayer().getPermissions()) list.add(permission);
			for(String permission : list) getProxiedPlayer().setPermission(permission, false);
			return;
		}
		for(PermissionAttachmentInfo attachmentInfo : getPlayer().getEffectivePermissions()) {
			PermissionAttachment attachment = attachmentInfo.getAttachment();
			if(attachment != null) {
				Map<String, Boolean> flags = attachment.getPermissions();
				for(String key : flags.keySet()) attachment.setPermission(key, false);
			}
		}
		return;
	}
	
	public class DataManager {
		
		private Mysql mysql = UtilsManager.getMysql();
		
		private ZentryPlayer player;
		
		public DataManager(ZentryPlayer player) {
			this.player = player;
		}
		
		public boolean dataExists() {
			ResultSet resultSet = null;
			try {
				resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`player_cache` WHERE `identifier` = '" + player.getIdentifier() + "';");
				if(resultSet.next()) {
					return true;
				}
				return false;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar informa��es de seu usu�rio do banco de dados.");
				return false;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
		}
		
		public boolean createData() {
			if(dataExists()) return false;
			try {
				long time = new Timestamp(System.currentTimeMillis()).getTime();
				String[] data = {
						player.getName(),
						player.getAddress(),
						"" + time,
						"" + time,
						"" + time,
						"" + false,
						"" + false,
						"" + true,
						"" + true,
				};
				mysql.getStatement().execute("INSERT INTO `" + mysql.getDatabase() + "`.`player_cache` (`identifier`, `data`) VALUES ('" + player.getIdentifier() + "', '" + UtilsManager.formatArray(data, ":") + "');");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel inserir seu usu�rio no banco de dados.");
			}
			return false;
		}
		
		public boolean deleteData() {
			if(!dataExists()) return false;
			try {
				mysql.getStatement().execute("DELETE FROM `" + mysql.getDatabase() + "`.`player_cache` WHERE `identifier` = '" + player.getIdentifier() + "';");
				player.kick("Seu usu�rio foi deletado do banco de dados.");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel deletar seu usu�rio do banco de dados.");
			}
			return false;
		}
		
		public String[] getData() {
			ResultSet resultSet = null;
			try {
				resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`player_cache` WHERE `identifier` = '" + player.getIdentifier() + "';");
				if(resultSet.next()) {
					return resultSet.getString("data").split(":");
				}
			} catch(Exception exception) {
				exception.printStackTrace();
				return null;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
			return null;
		}
		
		public String getUsername() {
			return getData()[0];
		}
		
		public String getAddress() {
			return getData()[1];
		}
		
		public long getFirstLogin() {
			return Long.valueOf(getData()[2]);
		}
		
		public long getLastLogin() {
			return Long.valueOf(getData()[3]);
		}
		
		public long getLastSeen() {
			return Long.valueOf(getData()[4]);
		}
		
		private boolean set(int position, Object value) {
			try {
				String[] data = getData();
				if(data[position] == null || data.length <= position) return false;
				data[position] = String.valueOf(value);
				mysql.getStatement().execute("UPDATE `" + mysql.getDatabase() + "`.`player_cache` SET `data` = '" + UtilsManager.formatArray(data, ":") + "' WHERE `identifier` = '" + player.getIdentifier() + "';");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel alterar algo em seu cache. #" + position);
			}
			return false;
		}
		
		public boolean setAddress(InetSocketAddress address) {
			return set(0, address.getHostString());
		}
		
		public boolean setUsername(String username) {
			return set(1, username);
		}
		
		public boolean setFirstLogin(Long firstLogin) {
			return set(2, "" + firstLogin);
		}
		
		public boolean setLastLogin(Long lastLogin) {
			return set(3, "" + lastLogin);
		}
		
		public boolean setLastSeen(Long lastSeen) {
			return set(4, "" + lastSeen);
		}
		
		public String formatFirstLogin() {
			return UtilsManager.getDateFormat().format(getFirstLogin());
		}
		
		public String formatLastLogin() {
			return UtilsManager.getDateFormat().format(getLastLogin());
		}
		
		public String formatLastSeen() {
			return getLastSeen() == 0 ? "Online" : UtilsManager.getDateFormat().format(getLastSeen());
		}
	}
	
	public class GroupManager {

		private Mysql mysql = UtilsManager.getMysql();
		
		private ZentryPlayer player;
		
		public GroupManager(ZentryPlayer player) {
			this.player = player;
		}
		
		public boolean dataExists() {
			ResultSet resultSet = null;
			try {
				resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`player_group` WHERE `identifier` = '" + player.getIdentifier() + "';");
				if(resultSet.next()) {
					return true;
				}
				return false;
			} catch(Exception exception) {
				exception.printStackTrace();
				return false;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
		}
		
		public boolean createData() {
			if(dataExists()) return false;
			try {
				long time = new Timestamp(System.currentTimeMillis()).getTime();
				String[] group = {
						Groups.MEMBRO.name(),
						"" + 0,
						"" + time,
						bungeecord ? ProxyServer.getInstance().getConsole().getName() : Bukkit.getConsoleSender().getName()
				};
				mysql.getStatement().execute("INSERT INTO `" + mysql.getDatabase() + "`.`player_group` (`identifier`, `data`, `permissions`) VALUES ('" + player.getIdentifier() + "', '" + UtilsManager.formatArray(group, ":") + "', '');");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel inserir seu grupo no banco de dados.");
			}
			return false;
		}
		
		public boolean deleteData() {
			if(!dataExists()) return false;
			try {
				mysql.getStatement().execute("DELETE FROM `" + mysql.getDatabase() + "`.`player_group` WHERE `identifier` = '" + player.getIdentifier() + "';");
				player.kick("Seu grupo foi deletado do banco de dados.");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel deletar seu grupo do banco de dados.");
			}
			return false;
		}
		
		public String[] getData() {
			ResultSet resultSet = null;
			try {
				resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`player_group` WHERE `identifier` = '" + player.getIdentifier() + "';");
				if(resultSet.next()) {
					return resultSet.getString("data").split(":");
				}
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar informa��es de seu grupo do banco de dados.");
				return null;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
			return null;
		}
		
		public String[] getPermissions() {
			ResultSet resultSet = null;
			try {
				resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`player_group` WHERE `identifier` = '" + player.getIdentifier() + "';");
				if(resultSet.next()) {
					return resultSet.getString("permissions").split(":");
				}
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar informa��es de suas permiss�es do banco de dados.");
				return null;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
			return null;
		}
		
		public List<String> getPermissionsList() {
			return new LinkedList<>(Arrays.asList(getPermissions()));
		}
		
		public boolean updatePermissions(List<String> permissions) {
			try {
				mysql.getStatement().execute("UPDATE `" + mysql.getDatabase() + "`.`player_group` SET `permissions` = '" + UtilsManager.formatArray(permissions.toArray(), ":") + "' WHERE `identifier` = '" + player.getIdentifier() + "';");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel atualizar suas permiss�es.");
			}
			return false;
		}
		
		public Groups getGroup() {
			try {
				return Groups.valueOf(getData()[0].toUpperCase());
			} catch(Exception exception) {
				player.kick("N�o foi poss�vel retornar seu grupo do banco de dados.");
			}
			return null;
		}
		
		public long getExpire() {
			try {
				return Long.valueOf(getData()[1]);
			} catch(Exception exception) {
				player.kick("N�o foi poss�vel retornar tempo restante de seu grupo do banco de dados.");
			}
			return 0;
		}
		
		public boolean isTemporary() {
			try {
				return Integer.valueOf(getData()[1]) > 0;
			} catch(Exception exception) {
				player.kick("N�o foi poss�vel verificiar a dura��o de seu grupo no banco de dados.");
			}
			return false;
		}
		
		public long getLastChange() {
			try {
				return Long.valueOf(getData()[2]);
			} catch(Exception exception) {
				player.kick("N�o foi poss�vel retornar �ltima altera��o do seu grupo.");
			}
			return 0;
		}
		
		public String getLastChanger() {
			return getData()[3];
		}
		
		public boolean hasPermission(String permission) {
			return getPermissionsList().contains(permission);
		}
		
		public void setPermission(String permission, boolean value) {
			List<String> permissions = getPermissionsList();
			if(value) {
				if(hasPermission(permission)) return;
				permissions.add(permission); updatePermissions(permissions);
				player.unloadPermissions(); player.injectPermissions();
			} else {
				if(!hasPermission(permission)) return;
				permissions.remove(permission); updatePermissions(permissions);
				player.unloadPermissions(); player.injectPermissions();
			}
		}
		
		private boolean set(int position, Object value) {
			try {
				String[] data = getData();
				if(data[position] == null || data.length <= position) return false;
				data[position] = String.valueOf(value);
				mysql.getStatement().execute("UPDATE `" + mysql.getDatabase() + "`.`player_group` SET `data` = '" + UtilsManager.formatArray(data, ":") + "' WHERE `identifier` = '" + player.getIdentifier() + "';");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel alterar algo em seu grupo. #" + position);
			}
			return false;
		}
		
		public boolean setGroup(Groups group) {
			return set(0, group);
		}
		
		public boolean setExpire(long expire) {
			return set(1, expire);
		}
		
		public boolean setLastChange(long lastChange) {
			return set(2, lastChange);
		}
		
		public boolean setLastChanger(String lastChanger) {
			return set(3, lastChanger);
		}
		
		public String formatLastChange() {
			return UtilsManager.getDateFormat().format(getLastChange());
		}
	}
	
	public class StatsManager {
		
		private Mysql mysql = UtilsManager.getMysql();
		
		private ZentryPlayer player;
		
		public StatsManager(ZentryPlayer player) {
			this.player = player;
		}
		
		public boolean dataExists() {
			ResultSet resultSet = null;
			try {
				resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`player_stats` WHERE `identifier` = '" + player.getIdentifier() + "';");
				if(resultSet.next()) {
					return true;
				}
				return false;
			} catch(Exception exception) {
				exception.printStackTrace();
				return false;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
		}
		
		public boolean createData() {
			if(dataExists()) return false;
			try {
				String[] group = {
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
						"" + 0,
				};
				mysql.getStatement().execute("INSERT INTO `" + mysql.getDatabase() + "`.`player_stats` (`identifier`, `data`) VALUES ('" + player.getIdentifier() + "', '" + UtilsManager.formatArray(group, ":") + "');");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel inserir seu status no banco de dados.");
			}
			return false;
		}
		
		public boolean deleteData() {
			if(!dataExists()) return false;
			try {
				mysql.getStatement().execute("DELETE FROM `" + mysql.getDatabase() + "`.`player_stats` WHERE `identifier` = '" + player.getIdentifier() + "';");
				player.kick("Seu status foi deletado do banco de dados.");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel deletar seu status do banco de dados.");
			}
			return false;
		}
		
		public String[] getData() {
			ResultSet resultSet = null;
			try {
				resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`player_stats` WHERE `identifier` = '" + player.getIdentifier() + "';");
				if(resultSet.next()) {
					return resultSet.getString("data").split(":");
				}
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar informa��es de seu status do banco de dados.");
				return null;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
			return null;
		}
		
		public int getKills() {
			int position = 0;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getDeaths() {
			int position = 1;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getStreak() {
			int position = 2;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getXp() {
			int position = 3;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getCoins() {
			int position = 4;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getBoxes() {
			int position = 5;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int get1v1Wins() {
			int position = 6;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int get1v1Loses() {
			int position = 7;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int get1v1Ties() {
			int position = 8;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int get1v1Matches() {
			int position = 9;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getChallengeEasy() {
			int position = 10;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getChallengeMedium() {
			int position = 11;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getChallengeHard() {
			int position = 12;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getMLGHits() {
			int position = 13;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public int getMLGErrors() {
			int position = 14;
			try {
				return Integer.parseInt(getData()[position]);
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar algo de seus status. #" + position);
			}
			return 0;
		}
		
		public Ranks getRank() {
			Ranks rank = Ranks.UNRANKED;
			for(Ranks ranks : Ranks.values()) {
				if(ranks.getXp() <= getXp()) {
					rank = ranks;
				}
			}
			return rank;
		}
		
		private boolean set(int position, Object value) {
			try {
				String[] data = getData();
				if(data[position] == null || data.length <= position) return false;
				data[position] = String.valueOf(value);
				mysql.getStatement().execute("UPDATE `" + mysql.getDatabase() + "`.`player_stats` SET `data` = '" + UtilsManager.formatArray(data, ":") + "' WHERE `identifier` = '" + player.getIdentifier() + "';");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel alterar algo em seu status. #" + position);
			}
			return false;
		}
		
		public boolean setKills(int kills) {
			return set(0, kills);
		}
		
		public boolean setDeaths(int deaths) {
			return set(1, deaths);
		}
		
		public boolean setStreak(int streak) {
			return set(2, streak);
		}
		
		public boolean setXp(int xp) {
			return set(3, xp);
		}
		
		public boolean setCoins(int coins) {
			return set(4, coins);
		}
		
		public boolean setBoxes(int boxes) {
			return set(5, boxes);
		}
		
		public boolean set1v1Wins(int wins) {
			return set(6, wins);
		}
		
		public boolean set1v1Loses(int loses) {
			return set(7, loses);
		}
		
		public boolean set1v1Ties(int ties) {
			return set(8, ties);
		}
		
		public boolean set1v1Matches(int matches) {
			return set(9, matches);
		}
		
		public boolean setChallengeEasy(int hits) {
			return set(10, hits);
		}
		
		public boolean setChallengeMedium(int hits) {
			return set(11, hits);
		}
		
		public boolean setChallengeHard(int hits) {
			return set(12, hits);
		}
		
		public boolean setMLGHits(int hits) {
			return set(13, hits);
		}
		
		public boolean setMLGErrors(int errors) {
			return set(14, errors);
		}
		
		public String format(int integer) {
			return NumberFormat.getInstance().format(integer);
		}
		
		public boolean setRank(Ranks rank) {
			if(getRank().equals(rank)) return false;
			return setXp(rank.getXp());
		}
	}
	
	public class PunishmentManager {
		
		private Mysql mysql = UtilsManager.getMysql();
		
		private ZentryPlayer player;
		
		public PunishmentManager(ZentryPlayer player) {
			this.player = player;
		}
		
		public boolean dataExists() {
			ResultSet resultSet = null;
			try {
				resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`player_punishment` WHERE `identifier` = '" + player.getIdentifier() + "';");
				if(resultSet.next()) {
					return true;
				}
				return false;
			} catch(Exception exception) {
				exception.printStackTrace();
				return false;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
		}
		
		public boolean createData() {
			if(dataExists()) return false;
			try {
				String[] punishment = {
						"" + 0,
						"" + 0,
						"" + 0,
						null,
						null,
						"" + 0,
				};
				mysql.getStatement().execute("INSERT INTO `" + mysql.getDatabase() + "`.`player_punishment` (`identifier`, `data`) VALUES ('" + player.getIdentifier() + "', '" + UtilsManager.formatArray(punishment, ":") + "');");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel inserir sua puni��o no banco de dados.");
			}
			return false;
		}
		
		public boolean deleteData() {
			if(!dataExists()) return false;
			try {
				mysql.getStatement().execute("DELETE FROM `" + mysql.getDatabase() + "`.`player_punishment` WHERE `identifier` = '" + player.getIdentifier() + "';");
				player.kick("Seu status foi deletado do banco de dados.");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel deletar sua puni��o do banco de dados.");
			}
			return false;
		}
		
		public String[] getData() {
			ResultSet resultSet = null;
			try {
				resultSet = mysql.getStatement().executeQuery("SELECT * FROM `" + mysql.getDatabase() + "`.`player_punishment` WHERE `identifier` = '" + player.getIdentifier() + "';");
				if(resultSet.next()) {
					return resultSet.getString("data").split(":");
				}
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel retornar informa��es de sua puni��o do banco de dados.");
				return null;
			} finally {
				try {
					resultSet.close();
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			}
			return null;
		}
		
		public boolean isBan() {
			try {
				return Integer.valueOf(getData()[0]) == 1;
			} catch(Exception exception) {
				player.kick("N�o foi poss�vel verificiar a dura��o de sua puni��o no banco de dados.");
			}
			return false;
		}
		
		public boolean isTemporary() {
			try {
				return Integer.valueOf(getData()[1]) == 1;
			} catch(Exception exception) {
				player.kick("N�o foi poss�vel verificiar a dura��o de sua puni��o no banco de dados.");
			}
			return false;
		}
		
		public long getExpire() {
			try {
				return Long.valueOf(getData()[2]);
			} catch(Exception exception) {
				player.kick("N�o foi poss�vel verificiar o tempo restante de sua puni��o no banco de dados.");
			}
			return 0;
		}
		
		public String getReason() {
			return getData()[3];
		}
		
		public String getPunisher() {
			return getData()[4];
		}
		
		public long getTime() {
			try {
				return Long.valueOf(getData()[5]);
			} catch(Exception exception) {
				player.kick("N�o foi poss�vel verificiar a data de sua puni��o no banco de dados.");
			}
			return 0;
		}
		
		public boolean isPunished() {
			return getReason() != null && !getReason().equals("null");
		}
		
		private boolean set(int position, Object value) {
			try {
				String[] data = getData();
				if(data[position] == null || data.length <= position) return false;
				data[position] = String.valueOf(value);
				mysql.getStatement().execute("UPDATE `" + mysql.getDatabase() + "`.`player_punishment` SET `data` = '" + UtilsManager.formatArray(data, ":") + "' WHERE `identifier` = '" + player.getIdentifier() + "';");
				return true;
			} catch(Exception exception) {
				exception.printStackTrace();
				player.kick("N�o foi poss�vel alterar algo em seu status. #" + position);
			}
			return false;
		}
		
		public boolean setBan(boolean ban) {
			return set(0, ban ? 1 : 0);
		}
		
		public boolean setTemporary(boolean temporary) {
			return set(1, temporary ? 1 : 0);
		}
		
		public boolean setExpire(long expire) {
			return set(2, expire);
		}
		
		public boolean setReason(String reason) {
			return set(3, reason);
		}
		
		public boolean setPunisher(String punisher) {
			return set(4, punisher);
		}
		
		public boolean setTime(long time) {
			return set(5, time);
		}
		
		public String formatTime() {
			return UtilsManager.getDateFormat().format(getTime());
		}
	}
	
	public static class TagManager {
		
		public static HashMap<UUID, Tags> tags = new HashMap<>();
		
		private ZentryPlayer player;
		
		public TagManager(ZentryPlayer player) {
			this.player = player;
		}
		
		public ZentryPlayer getPlayer() {
			return player;
		}
		
		public List<Tags> getAvailableTags() {
			List<Tags> list = new ArrayList<>();
			Tags tag = null;
			for(Tags tags : Tags.values()) {
				if(player.hasPermission(tags.getPermission())) { tag = tags; }
			}
			for(Tags tags : Tags.values()) { if(tags.ordinal() <= tag.ordinal()) list.add(tags); }
			return list;
		}
		
		public Tags getTag() {
			return tags.get(player.getUuid()) != null ? tags.get(player.getUuid()) : Tags.MEMBRO;
		}
		
		char[] alphabet = "zyxwvutsrqponmlkjihgfedcba".toCharArray();
		
		public void setTag(Tags tag) {
			tags.put(player.getUuid(), tag);
			updateNickname();
		}
		
		@SuppressWarnings("deprecation")
		public void updateNickname() {
			for(Player online : Bukkit.getOnlinePlayers()) {
				Scoreboard scoreboard = online.getScoreboard();
				if(scoreboard.equals(Bukkit.getScoreboardManager().getMainScoreboard())) scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
				String name = alphabet[getTag().ordinal()] + "" + alphabet[player.getStatsManager().getRank().ordinal()] + "-" + player.getName();
				Team team = scoreboard.getTeam(name);
				if(team == null) team = scoreboard.registerNewTeam(name);
				team.setPrefix(getTag().isPrefix() ? getTag().getColor() + "" + ChatColor.BOLD + getTag().name().replace("PLUS", "+") + " " + getTag().getColor() : getTag().getColor() + "");
				team.setSuffix(ChatColor.GRAY + " [" + player.getStatsManager().getRank().getColor() + player.getStatsManager().getRank().getPrefix() + ChatColor.GRAY + "]");
				team.addPlayer(player.getPlayer());
				online.setScoreboard(scoreboard);
			}
		}
		
		public void findTag() {
			List<Tags> list = new LinkedList<>(getAvailableTags());
			setTag(list.get(list.size() - 1));
		}
	}
}
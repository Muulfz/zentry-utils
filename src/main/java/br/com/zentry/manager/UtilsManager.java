package br.com.zentry.manager;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.plugin.PluginDescriptionFile;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.com.zentry.bungeecord.BungeeUtils;
import br.com.zentry.manager.connection.Mysql;
import br.com.zentry.spigot.ZentryUtils;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.PluginDescription;

public class UtilsManager {
	
	public static Map<UUID, JsonObject> responses = new HashMap<>();
	
	public static File folder;
	
	private static Mysql mysql;
	public static Mysql getMysql() {
		return mysql;
	}
	
	private static SimpleDateFormat dateFormat;
	public static SimpleDateFormat getDateFormat() {
		return dateFormat;
	}
	
	public static String[] messages = {
			"a",
			"b",
			"c"
	};
	
	public static String crypt(String string) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			BigInteger hash = new BigInteger(1, digest.digest(string.getBytes()));
			return hash.toString(17);
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
	
	public static BaseComponent buildComponent(String text, HoverEvent hover, ClickEvent click) {
		BaseComponent component = new TextComponent(text);
		component.setHoverEvent(hover);
		component.setClickEvent(click);
		return component;
	}
	
	public static long stringToLong(String paramOfString) {
		String[] arrayOfString;
		Integer templateInteger;
		Integer integer;
		String[] timeString = paramOfString.split(",");
		Integer day = 0, hour = 0, minute = 0, second = 0;
		
		templateInteger = (arrayOfString = timeString).length;
		for(integer = 0; integer < templateInteger; integer++) {
			String string = arrayOfString[integer];
			if(string.contains("d") || string.contains("D")) {
				day = Integer.valueOf(string.replace("d", "").replace("D", "")).intValue();
			}
			if(string.contains("h") || string.contains("H")) {
				hour = Integer.valueOf(string.replace("h", "").replace("H", "")).intValue();
			}
			if(string.contains("m") || string.contains("M")) {
				minute = Integer.valueOf(string.replace("m", "").replace("M", "")).intValue();
			}
			if(string.contains("s") || string.contains("S")) {
				second = Integer.valueOf(string.replace("s", "").replace("S", "")).intValue();
			}
		}
		return convert(day, hour, minute, second);
	}
	
	public static String formatLong(long paramOfLong) {
		String message = "";
		long now = System.currentTimeMillis();
		long diff = paramOfLong - now;
		int seconds = (int) (diff / 1000L);
		if (seconds >= 86400) {
			int days = seconds / 86400;
			seconds %= 86400;
			if (days == 1) {
				message = String.valueOf(message) + days + " dia ";
			} else {
				message = String.valueOf(message) + days + " dias ";
			}
		}
		if (seconds >= 3600) {
			int hours = seconds / 3600;
			seconds %= 3600;
			if (hours == 1) {
				message = String.valueOf(message) + hours + " hora ";
			} else {
				message = String.valueOf(message) + hours + " horas ";
			}
		}
		if (seconds >= 60) {
			int min = seconds / 60;
			seconds %= 60;
			if (min == 1) {
				message = String.valueOf(message) + min + " minuto ";
			} else {
				message = String.valueOf(message) + min + " minutos ";
			}
		}
		if (seconds >= 0) {
			if (seconds == 1) {
				message = String.valueOf(message) + seconds + " segundo";
			} else {
				message = String.valueOf(message) + seconds + " segundos";
			}
		}
		if(seconds < 0) {
			message = "Expirado";
		}
		return message;
	}

	public static long convert(long days, long hours, long minutes, long seconds) {
		long x = 0L;
		long minute = minutes * 60L;
		long hour = hours * 3600L;
		long day = days * 86400L;
		x = minute + hour + day + seconds;
		long time = System.currentTimeMillis() + x * 1000L;
		return time;
	}
	
	public static void loadFiles() {
		if(!folder.exists()) folder.mkdir();
		File file = new File(folder.getPath() + "/mysql.txt");
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		File logs = new File(folder.getPath() + "/logs");
		if(!logs.exists()) logs.mkdir();
		File formats = new File(logs.getPath() + "/formatted");
		if(!formats.exists()) formats.mkdir();
	}
	
	static {
		folder = new File("C:/ZentryMC");
		loadFiles();
		mysql = new Mysql();
		mysql.connect();
		mysql.setup();
		dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
	}
	
	public static String formatArray(Object[] array, String splitter) {
		String string = Arrays.toString(array);
		return string.replace("[", "").replace("]", "").replace(", ", splitter);
	}
	
	private boolean bungeecord;
	
	public UtilsManager(boolean bungeecord) {
		this.bungeecord = bungeecord;
	}
	
	public boolean isBungeecord() {
		return bungeecord;
	}
	
	public void log(Level level, String message) {
		if(bungeecord) {
			PluginDescription description = BungeeUtils.getPlugin().getDescription();
			System.out.println("[" + description.getName() + " v" + description.getVersion() + " - " + level.getName() + "]: " + message);
			return;
		}
		PluginDescriptionFile file = ZentryUtils.getPlugin().getDescription();
		System.out.println("[" + file.getName() + " v" + file.getVersion() + " - " + level.getName() + "]: " + message);
		return;
	}
	
	public UUID formatUuid(String uuid) {
		try {
			return UUID.fromString(uuid.substring(0, 8) + "-" + uuid.substring(8, 12) + "-" + uuid.substring(12, 16) + "-" + uuid.substring(16, 20) + "-" +uuid.substring(20, 32));
		} catch(Exception exception) {
			return null;
		}
	}
	
	public UUID getUuid(String uuid) {
		try {
			return UUID.fromString(uuid);
		} catch(Exception exception) {
			return null;
		}
	}
	
	public UUID getCachedUuid(String username) {
		ResultSet resultSet = null;
		try {
			resultSet = mysql.getStatement().executeQuery("SELECT * FROM `player_cache` WHERE `id`;");
			List<String> matches = new ArrayList<>();
			while(resultSet.next()) {
				if(resultSet.getString("data").split(":")[0].toLowerCase().equalsIgnoreCase(username.toLowerCase())) {
					matches.add(resultSet.getString("identifier"));
				}
			}
			if(matches.size() > 1 || matches.size() <= 0) {
				return null;
			}
			return formatUuid(matches.get(0));
		} catch(Exception exception) {
			exception.printStackTrace();
			return null;
		} finally {
			try {
				resultSet.close();
			} catch(Exception exception) {
				exception.printStackTrace();
				return null;
			}
		}
	}
	
	public boolean isOriginal(String nickname) {
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL("https://api.mojang.com/users/profiles/minecraft/" + nickname + "?at=" + new Timestamp(System.currentTimeMillis()).getTime()).openConnection();
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(new InputStreamReader((InputStream)connection.getContent()));
			JsonObject object = element.getAsJsonObject();
			if(object.toString().contains("id") && !object.toString().contains("error")) {
				return true;
			}
			return false;
		} catch(Exception exception) {}
		return false;
	}
	
	public boolean isOriginal(UUID uuid) {
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid.toString().replace("-", "")).openConnection();
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(new InputStreamReader((InputStream)connection.getContent()));
			JsonObject object = element.getAsJsonObject();
			if(object.toString().contains("id") && !object.toString().contains("error")) {
				responses.put(uuid, object);
				return true;
			}
			return false;
		} catch(Exception exception) {}
		return false;
	}
	
	public UUID getOnlineUuid(String nickname) {
		if(!isOriginal(nickname)) return null;
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL("https://api.mojang.com/users/profiles/minecraft/" + nickname + "?at=" + new Timestamp(System.currentTimeMillis()).getTime()).openConnection();
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(new InputStreamReader((InputStream)connection.getContent()));
			JsonObject object = element.getAsJsonObject();
			return formatUuid(object.get("id").getAsString());
		} catch(Exception exception) {}
		return null;
	}
	
	public String getOnlineNickname(UUID uuid) {
		if(!isOriginal(uuid)) return null;
		return responses.get(uuid).get("name").getAsString();
	}
}

package br.com.zentry.manager.connection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

import br.com.zentry.manager.UtilsManager;

public class Mysql {
	
	private String host, database, user, password;
	private int port;
	private Connection connection;
	private Statement statement;
	String[] credentials;
	
	public Statement getStatement() {
		return statement;
	}
	
	public String getDatabase() {
		return database;
	}
	
	private boolean problem = false;
	public boolean hasProblem() {
		return problem;
	}
	
	@SuppressWarnings("resource")
	public Mysql() {
		File file = new File(UtilsManager.folder.getPath() + "/mysql.txt");
		if(!UtilsManager.folder.exists() || !file.exists()) UtilsManager.loadFiles();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = reader.readLine();
			if(line == null) {
				System.out.println("O arquivo de credenciais retornou nulo.");
				problem = true;
				return;
			}
			String[] split = line.split(":");
			if(split == null || split.length <= 4) {
				System.out.println("Nao foi possivel retornar credenciais em formato correto.");
				problem = true;
				return;
			}
			host = split[0];
			port = Integer.valueOf(split[1]);
			database = split[2].toLowerCase();
			user = split[3];
			password = split[4];
			credentials = new String[]{
					host, database,
					user, password
			};
			for(String string : credentials) {
				if((string == null || string.equals("null")) || port <= 0) {
					System.out.println("Alguma das credentiais retornou nula. (" + line + ")");
					problem = true;
					break;
				}
			}
			return;
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		problem = true;
		return;
	}
	
	public synchronized void connect() {
		if(problem) {
			System.out.println("Nao foi possivel iniciar tentativa de conexao pois um problema foi detectado.");
			return;
		}
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Properties properties = new Properties();
			properties.setProperty("user", user); properties.setProperty("password", password); 
			properties.setProperty("useSSL", "true");
			properties.setProperty("autoReconnect", "true");
			connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=true", properties);
			statement = connection.createStatement();
			return;
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		problem = true;
		return;
	}
	
	public synchronized void disconnect() {
		try {
			statement.close();
			connection.close();
			return;
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return;
	}
	
	private String table(String name, String primaryKey, boolean autoIncrement, String... columns) {
		String command = "CREATE TABLE IF NOT EXISTS `" + database + "`.`" + name.toLowerCase() + "` (";
		StringBuilder builder = new StringBuilder();
		for(String string : columns) {
			if(builder.length() > 0) builder.append(", ");
			builder.append(string);
		}
		command += builder.toString().trim() + ", PRIMARY KEY(`" + primaryKey.toLowerCase() + "`)) ENGINE = InnoDB DEFAULT CHARSET = UTF8";
		command += (autoIncrement ? " AUTO_INCREMENT = 1" : "") + ";";
		return command;
	}
	
	public synchronized void setup() {
		try {
			statement.executeUpdate("USE `" + database + "`;");
			statement.executeUpdate(table("player_cache", "id", true, "`id` INT NOT NULL AUTO_INCREMENT", "`identifier` VARCHAR(64) NOT NULL", "`data` TEXT NOT NULL"));
			statement.executeUpdate(table("player_group", "identifier", false, "`identifier` VARCHAR(64) NOT NULL", "`data` TEXT NOT NULL", "`permissions` TEXT NOT NULL"));
			statement.executeUpdate(table("player_stats", "identifier", false, "`identifier` VARCHAR(64) NOT NULL", "`data` TEXT NOT NULL"));
			statement.executeUpdate(table("player_punishment", "id", true, "`id` INT NOT NULL AUTO_INCREMENT", "`identifier` VARCHAR(64) NOT NULL", "`data` TEXT NOT NULL"));
			statement.executeUpdate(table("global_reports", "id", true, "`id` INT NOT NULL AUTO_INCREMENT", "`reported` VARCHAR(32) NOT NULL", "`reason` TEXT NOT NULL", "`time` LONG NOT NULL", "`expire` LONG NOT NULL"));
			statement.executeUpdate(table("global_youtubers", "uuid", false, "`uuid` VARCHAR(64) NOT NULL", "`channel` TEXT NOT NULL", "`time` LONG NOT NULL"));
			statement.executeUpdate(table("global_authenticator", "id", true, "`id` INT NOT NULL AUTO_INCREMENT", "`uuid` VARCHAR(64) NOT NULL", "`key` TEXT NOT NULL", "`password` VARCHAR(32)", "`registered` BOOLEAN NOT NULL", "`logged` BOOLEAN NOT NULL"));
			return;
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		problem = true;
		return;
	}
}

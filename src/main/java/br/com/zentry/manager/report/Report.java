package br.com.zentry.manager.report;

public class Report {
	
	private String reported;
	private String reason;
	private long time;
	private long expire;
	
	public Report(String reported, String reason, long time, long expire) {
		this.reported = reported;
		this.reason = reason;
		this.time = time;
		this.expire = expire;
	}
	
	public String getReported() {
		return reported;
	}
	
	public String getReason() {
		return reason;
	}
	
	public long getTime() {
		return time;
	}
	
	public long getExpire() {
		return expire;
	}
}

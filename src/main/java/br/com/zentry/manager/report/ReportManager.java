package br.com.zentry.manager.report;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.scheduler.BukkitRunnable;

import br.com.zentry.manager.UtilsManager;
import br.com.zentry.spigot.ZentryUtils;

public class ReportManager {
	
	public static List<Report> getReports() {
		List<Report> list = new ArrayList<>();
		ResultSet resultSet = null;
		try {
			resultSet = UtilsManager.getMysql().getStatement().executeQuery("SELECT * FROM `" + UtilsManager.getMysql().getDatabase() + "`.`global_reports` WHERE `id`;");
			while(resultSet.next()) {
				String reported = resultSet.getString("reported"); String reason = resultSet.getString("reason"); long time = resultSet.getLong("time"); long expire = resultSet.getLong("expire");
				Report report = new Report(reported, reason, time, expire);
				list.add(report);
			}
			return list;
		} catch(Exception exception) {
		} finally {
			try {
				resultSet.close();
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
		return Collections.emptyList();
	}
	
	public static void checkReports() {
		new BukkitRunnable() {
			@Override
			public void run() {
				List<Report> list = getReports();
				for(Report report : list) {
					int seconds = (int)((report.getExpire() - System.currentTimeMillis()) / 1000L);
					if(seconds <= 0) {
						unregisterReport(report);
					}
				}
			}
		}.runTaskTimer(ZentryUtils.getPlugin(), 0, 20);
	}
	
	public static List<Report> getReportsByName(String name) {
		List<Report> list = new ArrayList<>();
		for(Report report : getReports()) {
			if(report.getReported().equalsIgnoreCase(name)) list.add(report);
		}
		return list;
	}
	
	public static boolean registerReport(Report report) {
		try {
			UtilsManager.getMysql().getStatement().execute("INSERT INTO `" + UtilsManager.getMysql().getDatabase() + "`.`global_reports` (`reported`, `reason`, `time`, `expire`) VALUES ('" + report.getReported() + "', '" + report.getReason() + "', '" + report.getTime() + "', '" + report.getExpire() + "');");
			return true;
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return false;
	}
	
	public static boolean unregisterReport(Report report) {
		ResultSet resultSet = null;
		try {
			resultSet = UtilsManager.getMysql().getStatement().executeQuery("SELECT * FROM `" + UtilsManager.getMysql().getDatabase() + "`.`global_reports` WHERE `reported` = '" + report.getReported() + "' AND `reason` = '" + report.getReason() + "';");
			if(!resultSet.next()) return false;
			UtilsManager.getMysql().getStatement().execute("DELETE FROM `" + UtilsManager.getMysql().getDatabase() + "`.`global_reports` WHERE `reported` = '" + report.getReported() + "' AND `reason` = '" + report.getReason() + "';");
			return true;
		} catch(Exception exception) {
			exception.printStackTrace();
		}
		return false;
	}
}

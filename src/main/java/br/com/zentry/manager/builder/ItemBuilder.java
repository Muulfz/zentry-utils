package br.com.zentry.manager.builder;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

public class ItemBuilder {
	
	@SuppressWarnings("deprecation")
	public static ItemStack leather(Material material, Color color, String displayName, String... lore) {
		ItemStack item = new ItemStack(material);
		LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
		meta.setColor(color);
		meta.setDisplayName(displayName);
		meta.setLore(Arrays.asList(lore));
		meta.spigot().setUnbreakable(true);
		item.setItemMeta(meta);
		return item;
	}
	
	@SuppressWarnings("deprecation")
	public static ItemStack leather(Material material, Color color, Map<Enchantment, Integer> enchantments, String displayName, String... lore) {
		ItemStack item = new ItemStack(material);
		for(Enchantment enchantment : enchantments.keySet()) {
			item.addEnchantment(enchantment, enchantments.get(enchantment));
		}
		LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
		meta.setColor(color);
		meta.setDisplayName(displayName);
		meta.setLore(Arrays.asList(lore));
		meta.spigot().setUnbreakable(true);
		item.setItemMeta(meta);
		return item;
	}
	
	@SuppressWarnings("deprecation")
	public static ItemStack head(int amount, String owner, String displayName, String... lore) {
		ItemStack item = new ItemStack(Material.SKULL_ITEM, amount, (byte)SkullType.PLAYER.ordinal());
		SkullMeta meta = (SkullMeta) item.getItemMeta();
		meta.setOwner(owner);
		meta.setDisplayName(displayName);
		meta.setLore(Arrays.asList(lore));
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack headTexture(int amount, String texture, String displayName, String... lore) {
		ItemStack item = new ItemStack(Material.SKULL_ITEM, amount, (byte)SkullType.PLAYER.ordinal());
		SkullMeta meta = (SkullMeta) item.getItemMeta();
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		profile.getProperties().put("textures", new Property("textures", texture));
		try {
			Field field = meta.getClass().getDeclaredField("profile");
			field.setAccessible(true);
			field.set(meta, profile);
		} catch(Exception exception) {}
		meta.setDisplayName(displayName);
		meta.setLore(Arrays.asList(lore));
		item.setItemMeta(meta);
		return item;
	}
	
	private ItemStack item;
	private ItemMeta meta;
	
	public ItemBuilder(Material material, int amount, int data) {
		item = new ItemStack(material, amount, (byte)data);
		meta = item.getItemMeta();
	}
	
	public ItemBuilder setDisplayName(Object displayName) {
		meta.setDisplayName(displayName.toString());
		return this;
	}
	
	public ItemBuilder setLore(String... lore) {
		meta.setLore(Arrays.asList(lore));
		return this;
	}
	
	public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
		item.addEnchantment(enchantment, level);
		return this;
	}
	
	public ItemBuilder removeEnchantment(Enchantment enchantment) {
		if(!item.containsEnchantment(enchantment)) return this;
		item.removeEnchantment(enchantment);
		return this;
	}
	
	@SuppressWarnings("deprecation")
	public ItemStack build() {
		meta.spigot().setUnbreakable(true);
		item.setItemMeta(meta);
		return item;
	}
}
